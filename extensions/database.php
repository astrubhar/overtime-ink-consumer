<?php

class Database {

	public $connection;
	public $result;
	private $server;
	private $username;
	private $password;
	private $database;
	private $persistant;
	private $container;
	private $placeholder;

	function __construct() {

		// Create Frequency Object
		$frequency = new frequency();

		// Load Configuration
		$this->server = @$frequency->database->server;
		$this->username = @$frequency->database->username;
		$this->password = @$frequency->database->password;
		$this->database = @$frequency->database->database;
		$this->persistant = @$frequency->database->persistant;
		$this->container = @$frequency->database->container;
		$this->placeholder = @$frequency->database->placeholder;

		// Set Defaults
		if (!isset($this->server) && !is_string($this->server)) $this->server = "localhost";
		if (!isset($this->persistant) && !is_bool($this->persistant)) $this->persistant = false;
		if (!isset($this->container) && !is_string($this->container)) $this->container = "object";
		if (!isset($this->placeholder) && !is_string($this->placeholder)) $this->placeholder = "?";

		// Establish Connection
		if ($this->persistant) {

			$this->connection = mysql_pconnect($this->server, $this->username, $this->password);

		} else {

			$this->connection = mysql_connect($this->server, $this->username, $this->password);

		}
		mysql_select_db($this->database, $this->connection);

	}

	function escape($value) {

		return mysql_real_escape_string($value);

	}

	function execute() {

		$arguments = func_get_args();

		$query = $arguments[0];

		unset($arguments[0]);

		foreach ($arguments as $value) {

			if (is_object($value) and get_class($value) == "model") return false;

			$value = str_replace($this->placeholder, "@@@@@MYSQLPLACEHOLDER@@@@@", $value);

			$query = preg_replace("/" . preg_quote($this->placeholder) . "/", preg_quote(mysql_real_escape_string($value), "/"), $query, 1, $verify);

			if ($verify != 1) return false;

		}

		$query = str_replace("@@@@@MYSQLPLACEHOLDER@@@@@", $this->placeholder, $query);

		$this->query = $query;
		$this->result = @mysql_query($query);

		return ($this->result ? true : false);

	}

	function row($field = "") {

		if ($this->result) {

			@mysql_data_seek($this->result, 0);

			if (!empty($field)) {

				return @mysql_result($this->result, 0, $field);

			} else {

				if ($this->container == "object") {

					return @mysql_fetch_object($this->result);

				} else {

					return @mysql_fetch_assoc($this->result);

				}

			}

		} else return false;

	}

	function rows() {

		if ($this->total() > 0) {

			mysql_data_seek($this->result, 0);

			$container = array();

			if ($this->container == "object") {

				while ($row = mysql_fetch_object($this->result)) $container[] = $row;
				$container = (object) $container;

			} else {

				while ($row = mysql_fetch_assoc($this->result)) $container[] = $row;

			}

			return $container;

		} else return false;

	}

	function fetch() {

		return mysql_fetch_assoc($this->result);

	}

	function total() {

		return ($this->result ? @mysql_num_rows($this->result) : false);

	}

	function affected() {

		return mysql_affected_rows($this->result);

	}

	function id() {

		return ($this->result ? mysql_insert_id() : false);

	}

	function error() {

		return ($this->result ? mysql_error() : false);

	}

	function alive() {

		return mysql_ping();

	}

	function __destruct() {

		if ($this->result) @mysql_free_result($this->result);

	}

}

?>
