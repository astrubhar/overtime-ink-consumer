<div class="container">
	<div class="row">

		<div class="col-md-3"></div>
		<div class="col-md-6" style="padding-bottom: 5px; margin-top: 43px;">

			<img src="elements/images/brand-large.png" style="display: table; margin: 0px auto 50px auto;">

			<h2>Do you need assistance?</h2>

			<p>We're happy to help our customers.</p>

			<p>For order questions, you can send us an email at <a href="mailto:orders@overtimeink.com">orders@overtimeink.com</a>.</p>

			<p>We're also available for general questions by phone at (612) 210-1598.</p>

			<p>For a full list of contacts, visit our <a href="http://www.overtimeink.com/contact.html" target="_blank">contacts page</a>.

		</div>
		<div class="col-md-3"></div>

	</div>
</div>
