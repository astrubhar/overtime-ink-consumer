<input type="hidden" name="highlight_terms" value="<?=$highlight?>">
<input type="hidden" name="refresh_url" value="orders/index/<?=$status?>/<?=$page?>/<?=urlencode($find)?>">
<input type="hidden" name="company_path" value="<?=$company_path?>">
<input type="hidden" name="show_id" value="<?=$show_id?>">

<div class="container">
	<div class="row">

		<div class="col-md-2" style="padding-bottom: 5px; margin-top: 43px;">

			<a data-action="create" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i> Create Order</a>

		</div>

		<div class="col-md-10">

			<ul class="nav nav-tabs small">
				<li class="<?=($status == "all" ? "active" : "")?>"><a href="orders/index/all/1/<?=urlencode($find)?>">All (<?=number_format($counts["all"], 0)?>)</a></li>
				<li class="<?=($status == "ordered" ? "active" : "")?>"><a href="orders/index/ordered/1/<?=urlencode($find)?>">Ordered (<?=number_format($counts["ordered"], 0)?>)</a></li>
				<li class="<?=($status == "supplied" ? "active" : "")?>"><a href="orders/index/supplied/1/<?=urlencode($find)?>">Supplied (<?=number_format($counts["supplied"], 0)?>)</a></li>
				<li class="<?=($status == "scheduled" ? "active" : "")?>"><a href="orders/index/scheduled/1/<?=urlencode($find)?>">Scheduled (<?=number_format($counts["scheduled"], 0)?>)</a></li>
				<li class="<?=($status == "completed" ? "active" : "")?>"><a href="orders/index/completed/1/<?=urlencode($find)?>">Completed (<?=number_format($counts["completed"], 0)?>)</a></li>
				<li class="<?=($status == "shipped" ? "active" : "")?>"><a href="orders/index/shipped/1/<?=urlencode($find)?>">Shipped (<?=number_format($counts["shipped"], 0)?>)</a></li>
			</ul>

			<table id="list" class="table table-striped" style="margin-top: -1px; table-layout: fixed; white-space: nowrap;">
				<tr>
					<th style="width: 160px;">Name</th>
					<th class="hidden-xs">Address</th>
					<th class="hidden-xs" style="width: 160px;">City/State/Country</th>
					<th class="hidden-md hidden-sm hidden-xs" style="width: 140px;">Ordered</th>
					<th class="hidden-md hidden-sm hidden-xs" style="width: 170px;" colspan="2">Ship Date</th>
					<th style="width: 100px;">Status</th>
					<th style="width: 60px;"></th>
				</tr>
				<? foreach ($orders as $order): ?>
				<tr data-id="<?=$order->id?>">
					<td><?=$order->user_id->firstname?> <?=$order->user_id->lastname?></td>
					<td class="hidden-xs"><?=$order->address?></td>
					<? list($country, $state) = explode("-", $order->state) ?>
					<td class="hidden-xs"><?=trim("{$order->city}, {$state}, {$country}", ", ")?></td>
					<td class="hidden-md hidden-sm hidden-xs"><?=$timeago->inWords($order->created, "now")?></td>
					<td class="hidden-md hidden-sm hidden-xs"><?=date("m/d/Y", strtotime($order->shipped))?></td>
					<td class="hidden-md hidden-sm hidden-xs"><?=date("g:i A", strtotime($order->shipped))?></td>
					<td><?=ucwords($order->status)?>
					<td><a href="#" data-action="view" data-id="<?=$order->id?>" class="btn btn-primary btn-xs">View</a></td>
				</tr>
				<? endforeach ?>
			</table>

			<div id="not_found" class="alert alert-warning text-center <? if (!empty($orders)): ?>hidden<? endif ?>">
				<span class="alert-link">No Orders Match Filters</span>
			</div>

			<? if ($pages > 1): ?>
			<ul class="pagination">

				<? if ($start > 1): ?>
				<li><a href="orders/index/<?=$status?>/1/<?=urlencode($find)?>">1</a></li>
				<? endif ?>

				<? for ($i = $start; $i <= $end; $i++): ?>
				<li<? if ($i == $page): ?> class="active"<? endif ?>><a href="orders/index/<?=$status?>/<?=$i?>/<?=urlencode($find)?>"><?=$i?></a></li>
				<? endfor ?>

				<? if ($end < $pages): ?>
				<li><a href="orders/index/<?=$status?>/<?=$pages?>/<?=urlencode($find)?>"><?=$pages?></a></li>
				<? endif ?>

			</ul>
			<? endif ?>

		</div>
	</div>
</div>

<div class="modal fade" id="modifyModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title">Create Order</h2>
			</div>

			<div class="modal-body" style="padding-top: 20px;">

				<form class="form-horizontal upload" role="form" enctype="multipart/form-data" method="post" action="orders_json/upload_file">

					<div class="row">

						<div class="col-sm-7">

							<div class="form-group">
								<label for="form_address" class="col-sm-3 control-label" id="form_address_label">Address</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="form_address">
								</div>
							</div>

							<div class="form-group">
								<label for="form_address" class="col-sm-3 control-label" id="form_name_label"></label>
								<div class="col-sm-9">
									<div class="input-group">
										<input type="text" class="form-control" id="form_city" placeholder="City" style="width: 30%;">
										<select class="form-control" id="form_state" style="width: 40%;">
											<option value="">State/Province</option>
											<optgroup label="United States">
												<option value="US-AL">Alabama</option>
												<option value="US-AK">Alaska</option>
												<option value="US-AZ">Arizona</option>
												<option value="US-AR">Arkansas</option>
												<option value="US-CA">California</option>
												<option value="US-CO">Colorado</option>
												<option value="US-CT">Connecticut</option>
												<option value="US-DE">Delaware</option>
												<option value="US-FL">Florida</option>
												<option value="US-GA">Georgia</option>
												<option value="US-HI">Hawaii</option>
												<option value="US-ID">Idaho</option>
												<option value="US-IL">Illinois</option>
												<option value="US-IN">Indiana</option>
												<option value="US-IA">Iowa</option>
												<option value="US-KS">Kansas</option>
												<option value="US-KY">Kentucky</option>
												<option value="US-LA">Louisiana</option>
												<option value="US-ME">Maine</option>
												<option value="US-MD">Maryland</option>
												<option value="US-MA">Massachusetts</option>
												<option value="US-MI">Michigan</option>
												<option value="US-MN">Minnesota</option>
												<option value="US-MS">Mississippi</option>
												<option value="US-MO">Missouri</option>
												<option value="US-MT">Montana</option>
												<option value="US-NE">Nebraska</option>
												<option value="US-NV">Nevada</option>
												<option value="US-NH">New Hampshire</option>
												<option value="US-NJ">New Jersey</option>
												<option value="US-NM">New Mexico</option>
												<option value="US-NY">New York</option>
												<option value="US-NC">North Carolina</option>
												<option value="US-ND">North Dakota</option>
												<option value="US-OH">Ohio</option>
												<option value="US-OK">Oklahoma</option>
												<option value="US-OR">Oregon</option>
												<option value="US-PA">Pennsylvania</option>
												<option value="US-RI">Rhode Island</option>
												<option value="US-SC">South Carolina</option>
												<option value="US-SD">South Dakota</option>
												<option value="US-TN">Tennessee</option>
												<option value="US-TX">Texas</option>
												<option value="US-UT">Utah</option>
												<option value="US-VT">Vermont</option>
												<option value="US-VA">Virginia</option>
												<option value="US-WA">Washington</option>
												<option value="US-WV">West Virginia</option>
												<option value="US-WI">Wisconsin</option>
												<option value="US-WY">Wyoming</option>
											</optgroup>
											<optgroup label="Canada">
												<option value="CA-AB">Alberta</option>
												<option value="CA-BC">British Columbia</option>
												<option value="CA-MB">Manitoba</option>
												<option value="CA-NB">New Brunswick</option>
												<option value="CA-NL">Newfoundland and Labrador</option>
												<option value="CA-NS">Nova Scotia</option>
												<option value="CA-ON">Ontario</option>
												<option value="CA-PE">Prince Edward Island</option>
												<option value="CA-SK">Saskatchewan</option>
											</optgroup>
											<optgroup label="US Territory">
												<option value="US-AS">American Samoa</option>
												<option value="US-DC">District of Columbia</option>
												<option value="US-FM">Federated States of Micronesia</option>
												<option value="US-GU">Guam</option>
												<option value="US-MH">Marshall Islands</option>
												<option value="US-MP">Northern Mariana Islands</option>
												<option value="US-PW">Palau</option>
												<option value="US-PR">Puerto Rico</option>
												<option value="US-VI">Virgin Islands</option>
											</optgroup>
											<optgroup label="US Military State">
												<option value="US-AEA">Armed Forces Africa</option>
												<option value="US-AA">Armed Forces Americas</option>
												<option value="US-AEC">Armed Forces Canada</option>
												<option value="US-AEE">Armed Forces Europe</option>
												<option value="US-AEM">Armed Forces Middle East</option>
												<option value="US-AP">Armed Forces Pacific</option>
											</optgroup>
											<optgroup label="CA Territory">
												<option value="CA-NT">Northwest Territories</option>
												<option value="CA-NU">Nunavut</option>
												<option value="CA-YT">Yukon</option>
											</optgroup>
										</select>
										<input type="text" class="form-control" id="form_postcode" placeholder="Post Code" style="width: 30%;">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label"></label>
								<div class="col-sm-9">
									<a data-action="toggle" class="btn btn-default" data-value="same_address"><i class="glyphicon glyphicon-unchecked"></i> Ship to My Saved Address</a>
								</div>
							</div>

							<div class="form-group">
								<label for="form_address" class="col-sm-3 control-label" id="form_address_label">Shipping Date</label>
								<div class="col-sm-9">
								    <div class="input-group date" id="form_shipped" style="width: 280px;">
								        <input type="text" class="form-control" placeholder="No Date">
								        <span class="input-group-addon">
								        	<span class="glyphicon-calendar glyphicon"></span>
								        </span>
								    </div>
								    <p class="help-block">Subject to approval. We'll email you.</p>
								</div>
							</div>

						</div>
						<div class="col-sm-5">

							<div class="form-group">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label for="form_attachment_input">Artwork Files</label>
									<ul class="list-group interactive-list" id="attachments" style="position: relative; margin-bottom: 0px;">
										<li class="list-group-item input"><input type="file" name="file_attachment" id="form_attachment_input"></li>
									</ul>
									<p class="help-block">For best results, upload vector artwork as Adobe Illustrator (.ai or .eps)
									or Corel Draw (.cdr) files. All fonts must be outlined.</p>
								</div>
								<div class="col-sm-1"></div>
							</div>

						</div>

					</div>

					<br><hr style="margin-top: 0px; border-top-width: 5px; border-top-style: dotted;">

					<div class="row">

						<div class="col-sm-12">

							<table id="cart_list" class="table table-striped items" style="margin: 0px;">
							<thead>
								<tr>
									<th></th>
									<th style="width: 90px;">Quantity</th>
									<th>Garment</th>
									<th style="width: 160px;">Color</th>
									<th style="width: 155px;">Decoration</th>
									<th style="width: 170px;">Artwork</th>
									<th style="width: 80px;"></th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot>
								<tr>
									<th></th>
									<th>
										<div class="form-group" style="margin: 0px;">
											<input type="text" class="form-control" id="form_quantity" placeholder="#">
										</div>
									</th>
									<th>
										<div class="form-group" style="margin: 0px;">
											<input type="text" class="form-control" id="form_garment" placeholder="Garment Name">
										</div>
									</th>
									<th>
										<div class="form-group" style="margin: 0px;">
											<input type="text" class="form-control color-picker" id="form_color" placeholder="Color">
										</div>
									</th>
									<th>
										<div class="form-group" style="margin: 0px;">
											<select class="form-control" id="form_decoration">
												<option value="">- Select -</option>
												<option value="screen-printing">Screen Printing</option>
												<option value="embroidery">Embroidery</option>
												<option value="vinyl">Vinyl</option>
											</select>
										</div>
									</th>
									<th>
										<div class="form-group" style="margin: 0px;">
											<select class="form-control" id="form_artwork">
												<option value="">- Select -</option>
												<option value="provided">I'll Attach Artwork</option>
												<option value="in-house">Create In-House (Use Special Instructions)</option>
											</select>
										</div>
									</th>
									<th><a href="#" data-action="add_item" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-plus"></i> Add</a></th>
								</tr>
							</tfoot>
							</table>

						</div>

					</div>

					<br><hr style="margin-top: 0px; border-top-width: 5px; border-top-style: dotted;">

					<div class="row">

						<div class="col-sm-12" style="padding: 0px 35px 0px 55px;">

							<div class="form-group">
								<label class="control-label">Special Instructions</label>
								<textarea class="form-control" id="form_customer_notes" style="margin-top: 5px; height: 80px;"></textarea>
								<p class="help-block">Custom artwork can be created for your job if needed. Artwork is billed at
								$40/hour. Include in the <b>Special Instructions</b> a description of what you are look for, the
								number of colors you would like to use, and a brief description of the overall job.</p>
							</div>

						</div>

					</div>

				</form>

			</div>

			<div class="modal-footer" id="form_actions">
				<button type="button" class="btn btn-default pull-left" data-dismiss="modal"></span>Cancel</button>
				<button type="button" class="btn btn-success" data-action="save"></span>Create Order</button>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 10px;">&times;</button>
				<h2 class="modal-title">Order #<span id="view_id"></span> <span id="view_status" class="label label-success pull-right" style="margin-top: 5px; margin-right: 10px;">New</span></h2>
			</div>

			<div class="modal-body" style="padding-top: 20px;">

				<form class="form-horizontal upload" role="form" enctype="multipart/form-data" method="post" action="orders_json/upload_file">

					<div class="row">

						<div class="col-sm-7">

							<input type="hidden" id="view_id" value="">

							<div class="form-group">
								<label for="view_address" class="col-sm-3 control-label" id="view_address_label">Address</label>
								<div class="col-sm-9">
									<p class="form-control-static" id="view_address"></p>
									<p class="form-control-static">
										<span id="view_city"></span>, <span id="view_state"></span>, <span id="view_postcode"></span>
									</p>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Ordered</label>
								<div class="col-sm-6">
									<p class="form-control-static" id="view_created"></p>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" id="view_shipped_label">Shipping</label>
								<div class="col-sm-6">
									<p class="form-control-static" id="view_shipped"></p>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label">Production</label>
								<div class="col-sm-6">
									<p class="form-control-static" id="view_scheduled"></p>
								</div>
							</div>

						</div>
						<div class="col-sm-5">

							<div class="form-group">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label>Artwork Files</label>
									<ul class="list-group interactive-list" id="view_attachments" style="position: relative; margin-bottom: 0px;">
									</ul>
								</div>
								<div class="col-sm-1"></div>
							</div>

							<div class="form-group">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<label>Proof of Concepts</label>
									<ul class="list-group interactive-list" id="view_concepts" style="position: relative; margin-bottom: 0px;">
									</ul>
								</div>
								<div class="col-sm-1"></div>
							</div>

							<div class="form-group">
								<div class="col-sm-1"></div>
								<div class="col-sm-10">
									<a class="btn btn-success" id="approve_concepts">Approve Proof of Concepts</a>
									<p class="help-block">If you find print issues, please <a href="help/index" target="_blank">contact us</a>.</p>
								</div>
								<div class="col-sm-1"></div>
							</div>

						</div>

					</div>

					<br><hr style="margin-top: 0px; border-top-width: 5px; border-top-style: dotted;">

					<div class="row">

						<div class="col-sm-12">

							<table id="view_cart_list" class="table table-striped items" style="margin: 0px;">
							<thead>
								<tr>
									<th></th>
									<th style="width: 90px;">Quantity</th>
									<th>Garment</th>
									<th style="width: 160px;">Color</th>
									<th style="width: 128px;">Decoration</th>
									<th style="width: 170px;">Artwork</th>
									<th style="width: 80px;"></th>
								</tr>
							</thead>
							<tbody></tbody>
							</table>

						</div>

					</div>

					<br><hr style="margin-top: 0px; border-top-width: 5px; border-top-style: dotted;">

					<div class="row">

						<div class="col-sm-12" style="padding: 0px 15px;">

							<div class="form-group" style="margin: 0px;">
								<label class="control-label">Special Instructions</label>
								<p class="form-control-static" id="view_customer_notes"></p>
							</div>

						</div>

					</div>

				</form>

			</div>

			<div class="modal-footer" id="view_actions">
				<a class="btn btn-success pull-left" href="help/index" target="_blank">Do you need assistance?</a>
				<button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
			</div>

		</div>
	</div>
</div>

<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<input type="hidden" name="id" value="">

			<div class="modal-body">

				<div style="width: 100%; text-align: center; margin: 60px 0px 80px 0px;">
					<h1 class="text-success"><i class="glyphicon glyphicon-ok"></i> Order #<span class="order_id"></span> Received</h1>
				</div>

				<p>We have successfully received your order.</p>

				<p>If you have any questions or concerns, you can contact us directly.

				<p><b>By Email:</b> <a href="mailto:orders@overtimeink.com">orders@overtimeink.com</a></p>

				<p><b>By Phone:</b> (612) 210-1598</p>

				<br><br><br>

				<button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>

				<div class="clearfix"></div>

			</div>

		</div>
	</div>
</div>

<script defer src="elements/scripts/orders.js"></script>