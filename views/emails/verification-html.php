<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Verify your account!</title>
</head>

<body style="background-color: #3E4651;">

	<br>

	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td width="10%"></td>
			<td width="80%" style="background-color: #FFFFFF; border-radius: 8px; padding: 20px;">

				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td style="padding-bottom: 20px; border-bottom: 1px solid #D4D4D4; font: 11pt helvetica, arial, sans-serif;">

							<img src="<?php echo $path ?>elements/images/email-logo.png" height="100" width="155">

						</td>
					</tr>
					<tr>
						<td style="padding: 40px 0px; border-bottom: 1px solid #D4D4D4; font: 11pt helvetica, arial, sans-serif;">

							<h1 style="color: #4C91CD;  font-size: 20pt;">Please confirm your email address!</h1>

							<p>This email address was recently registered on OverTime Ink. If this
							was you, click the following link to confirm your account:</p>

							<p><a href="<?php echo $path ?>session/confirm/<?php echo $id ?>/<?php echo $checksum ?>" style="color: #014EA5;"><?php echo $path ?>session/confirm/<?php echo $id ?>/<?php echo $checksum ?></a></p>

							<p>If you didn't register for this service, you can delete this email.</p>

						</td>
					</tr>
					<tr>
						<td style="padding-top: 20px; font: 11pt helvetica, arial, sans-serif; color: #999999;">

							You will not receive any additional emails unless you confirm this account.

						</td>
					</tr>
				</table>

			</td>
			<td width="10%"></td>
		</tr>
	</table>

</body>

</html>