OverTime Ink

----------------------------------------------------------------------

Verify your account!

----------------------------------------------------------------------

This email address was recently registered on OverTime Ink. If this
was you, click the following link to confirm your account:

<?php echo $path ?>session/confirm/<?php echo $id ?>/<?php echo $checksum ?>

If you didn't register for this service, you can disregard this email.

----------------------------------------------------------------------

Unsubscribe to stop receiving these emails.