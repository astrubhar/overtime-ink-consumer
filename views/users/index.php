<div class="container">
	<div class="row">

		<div class="col-md-3">

		</div>

		<div class="col-md-6">

			<form class="form-horizontal" action="users/index" method="post" role="form">

				<div class="form-group<? if (in_array("firstname", $errors) or in_array("lastname", $errors)): ?> has-error<? endif ?>">
					<label for="form_firstname" class="col-sm-3 control-label">Name</label>
					<div class="col-sm-8">
						<div class="input-group">
							<input type="text" class="form-control" name="firstname" placeholder="First" style="width: 50%;" value="<?=$user->firstname?>">
							<input type="text" class="form-control" name="lastname" placeholder="Last" style="width: 50%;" value="<?=$user->lastname?>">
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="form_company" class="col-sm-3 control-label">Company</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="company" placeholder="Optional" value="<?=$user->company?>">
					</div>
				</div>

				<div class="form-group<? if (in_array("email", $errors)): ?> has-error<? endif ?>">
					<label for="form_email" class="col-sm-3 control-label">Email</label>
					<div class="col-sm-8">
						<input type="email" class="form-control" name="email" value="<?=$user->email?>">
					</div>
				</div>

				<div class="form-group<? if (in_array("password", $errors)): ?> has-error<? endif ?>">
					<label for="form_password" class="col-sm-3 control-label">New Password</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" name="password" placeholder="At least 6 characters">
					</div>
				</div>

				<div class="form-group<? if (in_array("confirm_password", $errors)): ?> has-error<? endif ?>">
					<label for="form_confirm_password" class="col-sm-3 control-label">Verify Password</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" name="confirm_password">
					</div>
				</div>

				<div class="form-group<? if (in_array("address", $errors)): ?> has-error<? endif ?>">
					<label for="form_address" class="col-sm-3 control-label">Address</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="address" value="<?=$user->address?>">
					</div>
				</div>

				<div class="form-group">
					<label for="form_address" class="col-sm-3 control-label"></label>
					<div class="col-sm-8">
						<div class="input-group<? if (in_array("city", $errors) or in_array("state", $errors) or in_array("postcode", $errors)): ?> has-error<? endif ?>">
							<input type="text" class="form-control" name="city" placeholder="City" style="width: 30%;" value="<?=$user->city?>">
							<select class="form-control" name="state" style="width: 40%;">
								<? ob_start() ?>
								<option value="">State/Province</option>
								<optgroup label="United States">
									<option value="US-AL">Alabama</option>
									<option value="US-AK">Alaska</option>
									<option value="US-AZ">Arizona</option>
									<option value="US-AR">Arkansas</option>
									<option value="US-CA">California</option>
									<option value="US-CO">Colorado</option>
									<option value="US-CT">Connecticut</option>
									<option value="US-DE">Delaware</option>
									<option value="US-FL">Florida</option>
									<option value="US-GA">Georgia</option>
									<option value="US-HI">Hawaii</option>
									<option value="US-ID">Idaho</option>
									<option value="US-IL">Illinois</option>
									<option value="US-IN">Indiana</option>
									<option value="US-IA">Iowa</option>
									<option value="US-KS">Kansas</option>
									<option value="US-KY">Kentucky</option>
									<option value="US-LA">Louisiana</option>
									<option value="US-ME">Maine</option>
									<option value="US-MD">Maryland</option>
									<option value="US-MA">Massachusetts</option>
									<option value="US-MI">Michigan</option>
									<option value="US-MN">Minnesota</option>
									<option value="US-MS">Mississippi</option>
									<option value="US-MO">Missouri</option>
									<option value="US-MT">Montana</option>
									<option value="US-NE">Nebraska</option>
									<option value="US-NV">Nevada</option>
									<option value="US-NH">New Hampshire</option>
									<option value="US-NJ">New Jersey</option>
									<option value="US-NM">New Mexico</option>
									<option value="US-NY">New York</option>
									<option value="US-NC">North Carolina</option>
									<option value="US-ND">North Dakota</option>
									<option value="US-OH">Ohio</option>
									<option value="US-OK">Oklahoma</option>
									<option value="US-OR">Oregon</option>
									<option value="US-PA">Pennsylvania</option>
									<option value="US-RI">Rhode Island</option>
									<option value="US-SC">South Carolina</option>
									<option value="US-SD">South Dakota</option>
									<option value="US-TN">Tennessee</option>
									<option value="US-TX">Texas</option>
									<option value="US-UT">Utah</option>
									<option value="US-VT">Vermont</option>
									<option value="US-VA">Virginia</option>
									<option value="US-WA">Washington</option>
									<option value="US-WV">West Virginia</option>
									<option value="US-WI">Wisconsin</option>
									<option value="US-WY">Wyoming</option>
								</optgroup>
								<optgroup label="Canada">
									<option value="CA-AB">Alberta</option>
									<option value="CA-BC">British Columbia</option>
									<option value="CA-MB">Manitoba</option>
									<option value="CA-NB">New Brunswick</option>
									<option value="CA-NL">Newfoundland and Labrador</option>
									<option value="CA-NS">Nova Scotia</option>
									<option value="CA-ON">Ontario</option>
									<option value="CA-PE">Prince Edward Island</option>
									<option value="CA-SK">Saskatchewan</option>
								</optgroup>
								<optgroup label="US Territory">
									<option value="US-AS">American Samoa</option>
									<option value="US-DC">District of Columbia</option>
									<option value="US-FM">Federated States of Micronesia</option>
									<option value="US-GU">Guam</option>
									<option value="US-MH">Marshall Islands</option>
									<option value="US-MP">Northern Mariana Islands</option>
									<option value="US-PW">Palau</option>
									<option value="US-PR">Puerto Rico</option>
									<option value="US-VI">Virgin Islands</option>
								</optgroup>
								<optgroup label="US Military State">
									<option value="US-AEA">Armed Forces Africa</option>
									<option value="US-AA">Armed Forces Americas</option>
									<option value="US-AEC">Armed Forces Canada</option>
									<option value="US-AEE">Armed Forces Europe</option>
									<option value="US-AEM">Armed Forces Middle East</option>
									<option value="US-AP">Armed Forces Pacific</option>
								</optgroup>
								<optgroup label="CA Territory">
									<option value="CA-NT">Northwest Territories</option>
									<option value="CA-NU">Nunavut</option>
									<option value="CA-YT">Yukon</option>
								</optgroup>
								<?=str_replace("value=\"{$user->state}\"", "value=\"{$user->state}\" selected", ob_get_clean())?>
							</select>
							<input type="text" class="form-control" name="postcode" placeholder="Post Code" style="width: 30%;" value="<?=$user->postcode?>">
						</div>
					</div>
				</div>

				<div class="form-group<? if (in_array("phone", $errors)): ?> has-error<? endif ?>">
					<label for="form_phone" class="col-sm-3 control-label">Phone</label>
					<div class="col-sm-6">
						<div class="input-group">
							<input type="text" class="form-control" name="phone" style="width: 75%;" value="<?=($user->phone == 0 ? "" : preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $user->phone))?>">
							<input type="text" class="form-control" name="phone_ext" placeholder="Ext" style="width: 25%;" value="<?=($user->phone_ext == 0 ? "" : $user->phone_ext)?>">
						</div>
					</div>
				</div>

				<hr>

				<div class="form-group<? if (in_array("old_password", $errors)): ?> has-error<? endif ?>">
					<label for="form_password" class="col-sm-3 control-label">Old Password</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" name="old_password" placeholder="Required">
					</div>
				</div>

				<button type="submit" class="btn btn-success pull-right">Update</button>

			</form>

		</div>

		<div class="col-md-3">

		</div>

	</div>
</div>

<script defer src="elements/scripts/users.js"></script>