<input type="hidden" name="highlight_terms" value="<?=$highlight?>">
<!-- <input type="hidden" name="refresh_url" value="users/index/<?=$status?>/<?=$page?>/<?=urlencode($find)?>"> -->

<div class="container">
	<div class="row">

		<div class="col-md-3" style="padding-bottom: 5px;">

			<div id="events_view">

				<div id="events" class="list-group" style="margin-top: 77px;"></div>

			</div>

			<div id="orders_view" style="display: none;">

				<select class="form-control" id="status_filter" data-action="filter" data-url="calendar/index/%?/1/<?=urlencode($find)?>" style="margin-top: 20px;">
					<?=$this->replace()?>
					<option value="all">All Open Orders (<?=number_format($counts["open"], 0)?>)</option>
					<optgroup label="By Status">
						<option value="ordered">Ordered (<?=number_format($counts["ordered"], 0)?>)</option>
						<option value="supplied">Supplied (<?=number_format($counts["supplied"], 0)?>)</option>
						<option value="scheduled">Scheduled (<?=number_format($counts["scheduled"], 0)?>)</option>
						<option value="completed">Completed (<?=number_format($counts["completed"], 0)?>)</option>
						<option value="shipped">Shipped (<?=number_format($counts["shipped"], 0)?>)</option>
					</optgroup>
					<?=$this->replace("\"{$status}\"", "\"{$status}\" selected")?>
				</select>

				<div class="list-group" style="margin-top: 20px;">
					<? foreach ($orders as $order): ?>
					<a href="#" data-event-id="<?=$order->id?>" class="list-group-item" data-action="modify">
						<h4 class="list-group-item-heading"><?=$order->user_id->firstname?> <?=$order->user_id->lastname?> (#<?=$order->id?>)</h4>
						<p class="list-group-item-text">
							<? if (empty($order->items)): ?>&mdash;<? endif ?>
							<? foreach ($order->items as $item): ?>
							<?=$item->quantity?> &times; <?=$item->garment?><br>
							<? endforeach ?>
						</p>
					</a>
					<? endforeach ?>
				</div>

				<? if ($pages > 1): ?>
				<ul class="pager">
					<li class="previous<? if ($page >= $pages): ?> disabled<? endif ?>"><a href="<? if ($page >= $pages): ?>javascript:return false;<? else: ?>calendar/index/<?=$status?>/<?=($page + 1)?>/<?=urlencode($find)?><? endif ?>">&larr; Older</a></li>
					<li class="next<? if ($page < 2): ?> disabled<? endif ?>"><a href="<? if ($page < 2): ?>javascript:return false;<? else: ?>calendar/index/<?=$status?>/<?=($page - 1)?>/<?=urlencode($find)?><? endif ?>">Newer &rarr;</a></li>
				</ul>
				<? endif ?>

			</div>

			<div id="schedule_view" style="display: none;">

				<div class="well" id="form_details" style="margin-top: 20px; background: rgba(202,232,162,0.35);"></div>

				<input type="hidden" id="form_id" value="">

				<div class="form-group">
					<label for="form_start" class="control-label">Start Time</label>
				    <div class="input-group date" id="form_start">
				        <input type="text" class="form-control" placeholder="No Date">
				        <span class="input-group-addon">
				        	<span class="glyphicon-calendar glyphicon"></span>
				        </span>
				    </div>
				</div>

				<div class="form-group">
					<label for="form_end" class="control-label">End Time</label>
				    <div class="input-group date" id="form_end">
				        <input type="text" class="form-control" placeholder="No Date">
				        <span class="input-group-addon">
				        	<span class="glyphicon-calendar glyphicon"></span>
				        </span>
				    </div>
				</div>

				<div class="form-group" style="margin-bottom: 20px;">
					<label for="form_status" class="control-label">Status</label>
					<select class="form-control" id="form_status">
						<option value="ordered">Ordered</option>
						<option value="supplied">Garments Received</option>
						<option value="scheduled">Production Scheduled</option>
						<option value="completed">Production Completed</option>
						<option value="shipped">Shipped to Customer</option>
					</select>
				</div>

				<button type="button" class="btn btn-default pull-left" data-action="cancel"></span>Cancel</button>
				<button type="button" class="btn btn-success pull-right" data-action="save"></span>Save</button>

			</div>

		</div>

		<div class="col-md-9">

			<div class="row">

				<div class="col-md-12" style="padding-bottom: 5px; text-align: center;">

					<div class="btn-group pull-left">
						<button class="btn btn-default" data-calendar-nav="prev">&#171; Prev</button>
						<button class="btn btn-default" data-calendar-nav="today">Today</button>
						<button class="btn btn-default" data-calendar-nav="next">Next &#187;</button>
					</div>

					<div class="btn-group pull-right">
						<button class="btn btn-default" data-calendar-view="year">Year</button>
						<button class="btn btn-default active" data-calendar-view="month">Month</button>
						<button class="btn btn-default" data-calendar-view="week">Week</button>
						<button class="btn btn-default" data-calendar-view="day">Day</button>
					</div>

					<h3 id="title" style="margin-top: 5px;"></h3>

				</div>

			</div>

			<hr style="margin: 0px;">

			<div id="calendar"></div>

		</div>
	</div>
</div>

<script defer src="elements/scripts/calendar.js"></script>