<div class="container">
	<div class="row">

		<div class="col-md-3">

		</div>

		<div class="col-md-6">

			<div class="panel panel-default">
				<div class="panel-body">

					<form class="form-horizontal" action="session/register" method="post" role="form" id="registerForm">

						<h3 class="text-success">Register a New Account</h3>

						<p>Quickly create your account to place an order with OverTime Ink.</p>

						<hr>

						<div class="form-group">
							<label for="form_firstname" class="col-sm-3 control-label">Name</label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" class="form-control" id="form_firstname" placeholder="First" style="width: 50%;">
									<input type="text" class="form-control" id="form_lastname" placeholder="Last" style="width: 50%;">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="form_company" class="col-sm-3 control-label">Company</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="form_company" placeholder="Optional">
							</div>
						</div>

						<div class="form-group">
							<label for="form_email" class="col-sm-3 control-label">Email</label>
							<div class="col-sm-8">
								<input type="email" class="form-control" id="form_email">
							</div>
						</div>

						<div class="form-group">
							<label for="form_password" class="col-sm-3 control-label">Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" id="form_password" placeholder="At least 6 characters">
							</div>
						</div>

						<div class="form-group">
							<label for="form_confirm_password" class="col-sm-3 control-label">Verify Password</label>
							<div class="col-sm-8">
								<input type="password" class="form-control" id="form_confirm_password">
							</div>
						</div>

						<div class="form-group">
							<label for="form_address" class="col-sm-3 control-label">Address</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="form_address">
							</div>
						</div>

						<div class="form-group">
							<label for="form_address" class="col-sm-3 control-label"></label>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="text" class="form-control" id="form_city" placeholder="City" style="width: 30%;">
									<select class="form-control" id="form_state" style="width: 40%;">
										<option value="">State/Province</option>
										<optgroup label="United States">
											<option value="US-AL">Alabama</option>
											<option value="US-AK">Alaska</option>
											<option value="US-AZ">Arizona</option>
											<option value="US-AR">Arkansas</option>
											<option value="US-CA">California</option>
											<option value="US-CO">Colorado</option>
											<option value="US-CT">Connecticut</option>
											<option value="US-DE">Delaware</option>
											<option value="US-FL">Florida</option>
											<option value="US-GA">Georgia</option>
											<option value="US-HI">Hawaii</option>
											<option value="US-ID">Idaho</option>
											<option value="US-IL">Illinois</option>
											<option value="US-IN">Indiana</option>
											<option value="US-IA">Iowa</option>
											<option value="US-KS">Kansas</option>
											<option value="US-KY">Kentucky</option>
											<option value="US-LA">Louisiana</option>
											<option value="US-ME">Maine</option>
											<option value="US-MD">Maryland</option>
											<option value="US-MA">Massachusetts</option>
											<option value="US-MI">Michigan</option>
											<option value="US-MN">Minnesota</option>
											<option value="US-MS">Mississippi</option>
											<option value="US-MO">Missouri</option>
											<option value="US-MT">Montana</option>
											<option value="US-NE">Nebraska</option>
											<option value="US-NV">Nevada</option>
											<option value="US-NH">New Hampshire</option>
											<option value="US-NJ">New Jersey</option>
											<option value="US-NM">New Mexico</option>
											<option value="US-NY">New York</option>
											<option value="US-NC">North Carolina</option>
											<option value="US-ND">North Dakota</option>
											<option value="US-OH">Ohio</option>
											<option value="US-OK">Oklahoma</option>
											<option value="US-OR">Oregon</option>
											<option value="US-PA">Pennsylvania</option>
											<option value="US-RI">Rhode Island</option>
											<option value="US-SC">South Carolina</option>
											<option value="US-SD">South Dakota</option>
											<option value="US-TN">Tennessee</option>
											<option value="US-TX">Texas</option>
											<option value="US-UT">Utah</option>
											<option value="US-VT">Vermont</option>
											<option value="US-VA">Virginia</option>
											<option value="US-WA">Washington</option>
											<option value="US-WV">West Virginia</option>
											<option value="US-WI">Wisconsin</option>
											<option value="US-WY">Wyoming</option>
										</optgroup>
										<optgroup label="Canada">
											<option value="CA-AB">Alberta</option>
											<option value="CA-BC">British Columbia</option>
											<option value="CA-MB">Manitoba</option>
											<option value="CA-NB">New Brunswick</option>
											<option value="CA-NL">Newfoundland and Labrador</option>
											<option value="CA-NS">Nova Scotia</option>
											<option value="CA-ON">Ontario</option>
											<option value="CA-PE">Prince Edward Island</option>
											<option value="CA-SK">Saskatchewan</option>
										</optgroup>
										<optgroup label="US Territory">
											<option value="US-AS">American Samoa</option>
											<option value="US-DC">District of Columbia</option>
											<option value="US-FM">Federated States of Micronesia</option>
											<option value="US-GU">Guam</option>
											<option value="US-MH">Marshall Islands</option>
											<option value="US-MP">Northern Mariana Islands</option>
											<option value="US-PW">Palau</option>
											<option value="US-PR">Puerto Rico</option>
											<option value="US-VI">Virgin Islands</option>
										</optgroup>
										<optgroup label="US Military State">
											<option value="US-AEA">Armed Forces Africa</option>
											<option value="US-AA">Armed Forces Americas</option>
											<option value="US-AEC">Armed Forces Canada</option>
											<option value="US-AEE">Armed Forces Europe</option>
											<option value="US-AEM">Armed Forces Middle East</option>
											<option value="US-AP">Armed Forces Pacific</option>
										</optgroup>
										<optgroup label="CA Territory">
											<option value="CA-NT">Northwest Territories</option>
											<option value="CA-NU">Nunavut</option>
											<option value="CA-YT">Yukon</option>
										</optgroup>
									</select>
									<input type="text" class="form-control" id="form_postcode" placeholder="Post Code" style="width: 30%;">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="form_phone" class="col-sm-3 control-label">Phone</label>
							<div class="col-sm-6">
								<div class="input-group">
									<input type="text" class="form-control" id="form_phone" style="width: 75%;">
									<input type="text" class="form-control" id="form_phone_ext" placeholder="Ext" style="width: 25%;">
								</div>
							</div>
						</div>

						<hr>

						<a class="btn btn-default" href="session/authorize"><i class="glyphicon glyphicon-user"></i> Sign In</a>
						<a class="btn btn-success pull-right" data-action="register"><i class="glyphicon glyphicon-ok"></i> Register</a>

					</form>

				</div>
			</div>

		</div>

		<div class="col-md-3">

		</div>

	</div>
</div>

<div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<input type="hidden" name="id" value="">

			<div class="modal-header">
				<h4 class="modal-title">Account successfully created!</h4>
			</div>

			<div class="modal-body">

				<p>Your account has been created. Please check your inbox and
				verify your email address.</p>

			</div>

			<div class="modal-footer">
				<a class="btn btn-default pull-right" href="session/authorize"><i class="glyphicon glyphicon-user"></i> Sign In</a>
			</div>

		</div>
	</div>
</div>


<script defer src="elements/scripts/register.js"></script>