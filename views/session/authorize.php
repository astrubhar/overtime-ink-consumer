<div class="container">
	<div class="row">

		<div class="col-md-4">

		</div>

		<div class="col-md-4">

			<div class="hidden-sm hidden-xs" style="height: 100px;"></div>

			<? if ($flag == "verify"): ?>
			<div class="alert alert-danger">
				You need to verify your email before you can login. Check
				your inbox for a verification email (it was resent).
			</div>
			<? endif ?>

			<? if ($flag == "verified"): ?>
			<div class="alert alert-success">
				Your email address has been verified!
			</div>
			<? endif ?>

			<? if ($flag == "invalid"): ?>
			<div class="alert alert-danger">
				This is an invalid link.
			</div>
			<? endif ?>

			<div class="panel panel-default">
				<div class="panel-body">

					<form action="session/authorize/<?=$redirect?>" method="post" role="form">

						<div class="form-group<?=(in_array("email", $errors) ? " has-error" : "")?>">
							<input type="email" class="form-control" name="email" id="email" value="<?=(isset($_POST["email"]) ? $_POST["email"] : "")?>" placeholder="Email">
						</div>

						<div class="form-group<?=(in_array("password", $errors) ? " has-error" : "")?>">
							<input type="password" class="form-control" name="password" id="password" placeholder="Password">
						</div>

						<a class="btn btn-default" href="session/register"><i class="glyphicon glyphicon-user"></i> Register</a>
						<button type="submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-ok"></i> Sign In</button>

					</form>

				</div>
			</div>

		</div>

		<div class="col-md-4">

		</div>

	</div>
</div>