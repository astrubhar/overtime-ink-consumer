<?php

class Items_model extends Abstraction_model {

	public $abstraction_database   = "overtimeink";
	public $abstraction_table      = "items";
	public $abstraction_key        = "id";
	public $abstraction_savable    = true;
	public $abstraction_deletable  = true;
	public $abstraction_search     = true;
	public $abstraction_fields     = array(
		"id"          => "key",
		"order_id"    => "Orders_model",
		"quantity"    => "integer",
		"decoration"  => "enum",
		"color"       => "string",
		"garment"     => "string",
		"artwork"     => "enum",
	);
	public $abstraction_required   = array(
	);

}