<?php

class Users_model extends Abstraction_model {

	public $abstraction_database   = "overtimeink";
	public $abstraction_table      = "users";
	public $abstraction_key        = "id";
	public $abstraction_savable    = true;
	public $abstraction_deletable  = true;
	public $abstraction_search     = true;
	public $abstraction_fields     = array(
		"id"          => "key",
		"email"       => "email",
		"password"    => "string",
		"firstname"   => "string",
		"lastname"    => "string",
		"company"     => "string",
		"phone"       => "number",
		"phone_ext"   => "number",
		"address"     => "string",
		"city"        => "string",
		"state"       => "enum",
		"postcode"    => "string",
		"notes"       => "text",
		"role"        => "enum",
		"options"     => "set",
		"created"     => "datetime",
		"updated"     => "datetime",
		"last_login"  => "datetime",
	);
	public $abstraction_required   = array(
		"firstname",
		"lastname",
		"role",
		"email",
		"password",
		"firstname",
		"lastname",
		"phone",
		"address",
		"city",
		"state",
		"postcode",
		"role",
	);

	public function id_by_authentication($email, $password) {

		// Instantiate
		$frequency = new Frequency();
		$database  = new Database();

		// Hash Password
		$password = $this->generate_hash($email, $password);

		// Try to Match User
		$database->execute("

			SELECT
				`?`
			FROM
				`?`.`?`
			WHERE
				`email` = '?' AND
				`password` = '?'

		", $this->abstraction_key, $this->abstraction_database, $this->abstraction_table, $email, $password);

		// Return
		return ($database->total() > 0 ? $database->row($this->abstraction_key) : false);

	}

	public function generate_hash($email, $password) {

		// Return Cryptographic Hash
		return crypt($password, '$6$rounds=500000$' . substr(hash('sha512', $email), 18, 8) . substr(hash('sha512', $email), 31, 8) . '$');

	}

	public function email_exists($email) {

		// Instantiate
		$frequency = new Frequency();
		$database  = new Database();

		// Try to Match User
		$database->execute("

			SELECT
				`?`
			FROM
				`?`.`?`
			WHERE
				`email` = '?'

		", $this->abstraction_key, $this->abstraction_database, $this->abstraction_table, $email);

		// Return
		return ($database->total() > 0 ? true : false);

	}

	public function verification_email() {

		// Instantiate
		$frequency = new Frequency();
		$email     = new Email();
		$text_body = new View("emails/verification-text");
		$html_body = new View("emails/verification-html");

		// Set HTML View
		$html_body->path     = $frequency->application->path->desktop;
		$html_body->id       = $this->id;
		$html_body->checksum = substr(hash("sha512", "verify" . $frequency->application->seed . $this->id), 32, 10);

		// Set Text View
		$text_body->path     = $frequency->application->path->desktop;
		$text_body->id       = $this->id;
		$text_body->checksum = substr(hash("sha512", "verify" . $frequency->application->seed . $this->id), 32, 10);

		// Set From
		$from = array(
			"name"  => $frequency->application->name,
			"email" => $frequency->application->reply,
		);

		// Set Recipient
		$recipient = array(array(
			"name"  => "{$this->firstname} {$this->lastname}",
			"email" => $this->email,
		));

		// Send Email
		$email->send($from, $recipient, "Verify your account!", $text_body->export(), $html_body->export());

	}

	public function confirm_email($checksum) {

		// Instantiate
		$frequency = new Frequency();

		// Checksum valid?
		if ($checksum == substr(hash("sha512", "verify" . $frequency->application->seed . $this->id), 32, 10)) {

			// Needs verified?
			if (!in_array("verified", $this->options)) {

				// Add Option
				$this->options[] = "verified";

			}

			// Save
			$this->save();

			// Success
			return true;

		}

		// Failed
		return false;

	}

}
