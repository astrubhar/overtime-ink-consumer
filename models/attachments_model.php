<?php

class Attachments_model extends Abstraction_model {

	public $abstraction_database   = "overtimeink";
	public $abstraction_table      = "attachments";
	public $abstraction_key        = "id";
	public $abstraction_savable    = true;
	public $abstraction_deletable  = true;
	public $abstraction_search     = true;
	public $abstraction_fields     = array(
		"id"          => "key",
		"order_id"    => "integer",
		"name"        => "string",
		"extension"   => "string",
		"size"        => "float",
		"location"    => "string",
		"type"        => "enum",
	);
	public $abstraction_required   = array(
		"name",
		"extension",
		"size",
		"location",
		"type",
	);

	function upload($file) {

		// Instantiate
		$frequency = new Frequency();

		// Set Extension
		$extension = strtolower(pathinfo($file["name"], PATHINFO_EXTENSION));

		// Allowed extension?
		if (!in_array($extension, (array) $frequency->application->disallowed_uploads)) {

			// Set Maximum Retry Attempts
			$retry = 10;

			// Attempt to Generate Unique Filename
			while ($retry--) {

				// Generate Name
				$filename = substr(md5(uniqid(rand(1, 999999))), 5, 20) . "." . $extension;

				// Unique?
				if (!is_file("{$frequency->application->uploads}/{$filename}")) {

					// Break From Loop
					break;

				}

			}

			// Set Destination
			$destination = "{$frequency->application->uploads}/{$filename}";

			// Move File
			move_uploaded_file($file["tmp_name"], $destination);

			// Set Size
			$size = number_format(filesize($destination) / (1024 * 1024), 1);

			// Set Properties
			$this->name      = substr($file["name"], 0, 15);
			$this->extension = strtoupper($extension);
			$this->size	     = ($size < 0.1 ? "0.1" : $size);
			$this->location  = $filename;
			$this->type      = "attachment";

			// Save
			$this->save();

		}

	}

	function delete() {

		// Instantiate
		$frequency = new Frequency();

		// Remove File
		unlink("{$frequency->application->uploads}/{$this->location}");

		// Call Parent
		parent::delete();

	}

}