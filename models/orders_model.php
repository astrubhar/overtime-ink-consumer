<?php

class Orders_model extends Abstraction_model {

	public $abstraction_database   = "overtimeink";
	public $abstraction_table      = "orders";
	public $abstraction_key        = "id";
	public $abstraction_savable    = true;
	public $abstraction_deletable  = true;
	public $abstraction_search     = true;
	public $abstraction_fields     = array(
		"id"              => "key",
		"user_id"         => "Users_model",
		"address"         => "string",
		"city"            => "string",
		"state"           => "enum",
		"postcode"        => "string",
		"customer_notes"  => "text",
		"internal_notes"  => "text",
		"status"          => "enum",
		"options"         => "set",
		"start"           => "datetime",
		"end"             => "datetime",
		"created"         => "datetime",
		"updated"         => "datetime",
		"shipped"         => "datetime",
	);
	public $abstraction_required   = array(
		"user_id",
		"address",
		"city",
		"state",
		"postcode",
		"status",
		"shipped",
	);

	public function counts() {

		// Instantiate
		$database  = new Database();

		// Clear
		$database->execute("

			SELECT
				COUNT(*)                                              AS `all_count`,
				SUM(CASE WHEN `status` != 'shipped' THEN 1 ELSE 0 END)   `open_count`,
				SUM(CASE WHEN `status` = 'ordered' THEN 1 ELSE 0 END)    `ordered_count`,
				SUM(CASE WHEN `status` = 'supplied' THEN 1 ELSE 0 END)   `supplied_count`,
				SUM(CASE WHEN `status` = 'scheduled' THEN 1 ELSE 0 END)  `scheduled_count`,
				SUM(CASE WHEN `status` = 'completed' THEN 1 ELSE 0 END)  `completed_count`,
				SUM(CASE WHEN `status` = 'shipped' THEN 1 ELSE 0 END)    `shipped_count`
			FROM
				`?`.`?`
			WHERE
				`orders`.`user_id` = '?'

		", $this->abstraction_database, $this->abstraction_table, $_SESSION["id"]);

		// Return
		return array(
			"all"        => $database->row("all_count"),
			"open"       => $database->row("open_count"),
			"ordered"    => $database->row("ordered_count"),
			"supplied"   => $database->row("supplied_count"),
			"scheduled"  => $database->row("scheduled_count"),
			"completed"  => $database->row("completed_count"),
			"shipped"    => $database->row("shipped_count"),
		);

	}

	public function confirm_review($checksum) {

		// Instantiate
		$frequency = new Frequency();

		// Checksum valid?
		if ($checksum == substr(hash("sha512", "review" . $frequency->application->seed . $this->id), 32, 10)) {

			// Success
			return true;

		}

		// Failed
		return false;

	}

}
