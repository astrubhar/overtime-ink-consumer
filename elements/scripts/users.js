$(document).ready(function () {

	/**
	 *
	 *  Intervals
	 *
	**/

	// Every Minute
	setInterval(function () {

		// Refresh Page
		$("body").trigger("refresh");

	}, 60000);

	/**
	 *
	 *  Interaction Events
	 *
	**/

	// Delete Button
	$(document).on("click", "[data-action=delete]", function (event) {

		// Show Modal
		$("#deleteModal").trigger("show", [{
			"id"    : $(this).attr("data-id"),
			"label" : $(this).attr("data-label")
		}]);

		// Cancel Event
		event.preventDefault();

	});

	// Confirm Delete Button
	$(document).on("click", "[data-action=confirm_delete]", function (event) {

		// Send Request
		$.ajax({
			url: "users_json/delete/" + $("#deleteModal").find("input[name=id]").val(),
			dataType: "json",
			success: function(data) {

				// Successful?
				if (data.status == true) {

					// Refresh Page
					$("body").trigger("refresh");

					// Dismiss Modal
					$("#deleteModal").modal("hide");

				}

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	// Create Button
	$(document).on("click", "[data-action=create]", function (event) {

		// Show Modal
		$("#modifyModal").trigger("show", [{}]);

		// Cancel Event
		event.preventDefault();

	});

	// Modify Button
	$(document).on("click", "[data-action=modify]", function (event) {

		// Send Request
		$.ajax({
			url: "users_json/edit/" + $(this).attr("data-id"),
			dataType: "json",
			success: function(response) {

				// Show Modal
				$("#modifyModal").trigger("show", [response]);

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	// Save Button
	$("[data-action=save]").on("click", function (event) {

		// Get Values
		values = $("#modifyModal").triggerHandler("get");

		// Send Request
		$.ajax({
			type: "POST",
			url: "users_json/save/" + values.id,
			data: values,
			dataType: "json",
			success: function(data) {

				// Errors?
				if (data.errors.length > 0) {

					// Clear Errors
					$("#modifyModal").find(".form-group").removeClass("has-error");

					// Iterate Errors
					$.each(data.errors, function(index, value) {

						// Mark Error
						$("#modifyModal").find("#form_" + value).parents(".form-group").addClass("has-error");

					});

				}

				// Success?
				else {

					// Refresh Page
					$("body").trigger("refresh");

					// Dismiss Modal
					$("#modifyModal").modal("hide");

				}

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	// Confirm Staff and Administrator
	$(document).on("change", "#form_role", function (event) {

		// Special type?
		if ($(this).val() == "staff" || $(this).val() == "administrator") {

			// Changed mind?
			if (!confirm("Set role to " + $(this).val().toUpperCase() + "?\n\n(This user will have access to company application.)")) {

				// Set Back
				$(this).val("customer");

			}

		}

	});

	// Update Send Email Icon
	$(document).on("keyup", "[data-action=update_email]", function (event) {

		// Update
		$("#modifyModal").trigger("update_email", [
			$(this).val()
		]);

	});

	// Erase User Password
	$(document).on("click", "[data-action=erase_password]", function (event) {

		// Update
		$("#modifyModal").trigger("erase_password");

		// Cancel Event
		event.preventDefault();

	});

	/**
	 *
	 *  View Controllers
	 *
	**/

	// Refresh
	$("body").on("refresh", function (event) {

		$.ajax({
			type: "GET",
			url: $("input[name=refresh_url]").val(),
			data: {},
			dataType: "text",
			success: function(html) {

				// Replace Title
				$("title").text(html.match("<title>(.*?)</title>")[1].replace("&#187;", "»"));

				// Parse HTML DOM (Strips <head>)
				html = $.parseHTML(html);

				// Replace Table
				$("table.table").replaceWith($(html).find("table.table"));

				// Replace Pagination
				$("ul.pagination").replaceWith($(html).find("ul.pagination"));

				// Count Rows in Table
				if ($("table.table tr[data-id]").length > 0) {

					// Hide Not Found
					$("#not_found").addClass("hidden");

				} else {

					// Show Not Found
					$("#not_found").removeClass("hidden");

				}

				// Highlight Search Terms
				highlightSearchWords();

			}
		});

	});

	// Delete Modal -> Show
	$("#deleteModal").on("show", function (event, values) {

		// Set ID
		$(this).find("input[name=id]").val(values.id);

		// Set Label
		$(this).find("div.modal-body").html(values.label);

		// Show
		$(this).modal();

	});

	// Modify Modal -> Show
	$("#modifyModal").on("show", function (event, values) {

		// Remove Errors
		$(this).find(".form-group").removeClass("has-error");

		// Edit?
		if (typeof values.id !== "undefined") {

			// Set Values
			$(this).find("#form_id").val(values.id);
			$(this).find("#form_email").val(values.email);
			$(this).find("#form_firstname").val(values.firstname);
			$(this).find("#form_lastname").val(values.lastname);
			$(this).find("#form_company").val(values.company);
			$(this).find("#form_phone").val(values.phone != 0 ? values.phone : "");
			$(this).find("#form_phone_ext").val(values.phone_ext != 0 ? values.phone_ext : "");
			$(this).find("#form_address").val(values.address);
			$(this).find("#form_city").val(values.city);
			$(this).find("#form_state").val(values.state);
			$(this).find("#form_postcode").val(values.postcode);
			$(this).find("#form_notes").val($("<div />").html(values.notes).text());
			$(this).find("#form_role").val(values.role);
			$(this).find("#form_created").html(values.created);
			$(this).find("#form_updated").html(values.updated);
			$(this).find("#form_last_login").html(values.last_login);

			// Set Verified Toggle
			set_toggle_state($(this).find("a[data-value=verified]"), $.inArray("verified", values.options) > -1);

			// Set Email
			$(this).trigger("update_email", [values.email]);

			// Set Password
			$(this).trigger("update_password", [values.password]);

		} else {

			// Reset Form
			$(this).find("input,select,textarea").val("");

			// Set Empty Dates
			$(this).find("#form_created,#form_updated,#form_last_login").html("No Date");

			// Set Verified Toggle
			set_toggle_state($(this).find("a[data-value=verified]"), false);

			// Clear Email
			$(this).trigger("update_email", [""]);

			// Clear Password
			$(this).trigger("update_password", [false]);

		}

		// Show
		$(this).modal();

	});

	// Modify Modal -> Get Values
	$("#modifyModal").on("get", function (event) {

		// Define
		options = [];

		// Verified?
		if (is_toggle_checked($(this).find("a[data-value=verified]"))) options.push("verified");

		// Return Values
		return {
			"id"         : $(this).find("#form_id").val(),
			"email"      : $(this).find("#form_email").val(),
			"password"   : ($(this).triggerHandler("has_password") ? $(this).find("#form_password").val() : "NULL"),
			"firstname"  : $(this).find("#form_firstname").val(),
			"lastname"   : $(this).find("#form_lastname").val(),
			"company"    : $(this).find("#form_company").val(),
			"phone"      : $(this).find("#form_phone").val().replace(/[^\d]/g, ''),
			"phone_ext"  : $(this).find("#form_phone_ext").val(),
			"address"    : $(this).find("#form_address").val(),
			"city"       : $(this).find("#form_city").val(),
			"state"      : $(this).find("#form_state").val(),
			"postcode"   : $(this).find("#form_postcode").val(),
			"notes"      : $(this).find("#form_notes").val(),
			"role"       : $(this).find("#form_role").val(),
			"options"    : options
		};

	});

	// Modify Modal -> Update Email
	$("#modifyModal").on("update_email", function (event, email) {

		// Create Pattern
		email_pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

		// Valid email?
		if (email_pattern.test(email)) {

			// Update Link Reference
			$(this).find("#form_send_email").removeClass("disabled").attr("href", "mailto:" + email);

		} else {

			// Disable
			$(this).find("#form_send_email").addClass("disabled");

		}

	});

	// Modify Modal -> Update Password
	$("#modifyModal").on("update_password", function (event, has_password) {

		// Has password?
		if (has_password) {

			$(this).find("#form_password").attr("placeholder", "Change Password");
			$(this).find("#form_erase_password").removeClass("disabled");

		} else {

			$(this).find("#form_password").attr("placeholder", "Set Password");
			$(this).find("#form_erase_password").addClass("disabled");

		}

	});

	// Modify Modal -> Erase Password
	$("#modifyModal").on("erase_password", function (event) {

		// Clear
		$(this).find("#form_password").val("");

		// Update Password
		$(this).trigger("update_password", [false]);

	});

	// Modify Modal -> Update Password
	$("#modifyModal").on("has_password", function (event) {

		// Has password?
		if ($(this).find("#form_password").attr("placeholder") == "Set Password" && $(this).find("#form_password").val() == "") {

			return false;

		} else {

			return true;

		}

	});

});