var calendar;

$(document).ready(function () {

	/**
	 *
	 *  Interaction Events
	 *
	**/

	// Calendar Prev/Today/Next Nagivation
	$('.btn-group button[data-calendar-nav]').on("click", function () {

		// Set Calendar Navigation
		calendar.navigate($(this).attr('data-calendar-nav'));

	});

	// Calendar Year/Month/Week/Day View
	$('.btn-group button[data-calendar-view]').on("click", function () {

		// Set Calendar View
		calendar.view($(this).attr('data-calendar-view'));

	});

	/**
	 *
	 *  View Controllers
	 *
	**/

	// Refresh
	$("body").on("refresh", function (event) {

		// Create Calendar
		calendar = $('#calendar').calendar({
			events_source: 'calendar_json/events/',
			view: 'month',
			tmpl_path: 'elements/templates/calendar/',
			tmpl_cache: false,
			onAfterEventsLoad: function(events) {
				if(!events) {
					return;
				}
				var list = $('#events');
				list.html('');

				if (events.length < 1) {
					$('<h4>No Orders</h4>').appendTo(list);
				}

				$.each(events, function(key, val) {
					$('<span class="list-group-item"><span class="pull-left event ' + val["class"] + '" style="margin: 4px 4px 0px 0px;"></span> ' + val.title + '</span>')
						.appendTo(list);
				});
			},
			onAfterViewLoad: function(view) {
				$('h3#title').text(this.getTitle());
				$('.btn-group button').removeClass('active');
				$('button[data-calendar-view="' + view + '"]').addClass('active');
			},
			classes: {
				months: {
					general: 'label'
				}
			}
		});

	});

	// Load Calendar Immediately
	$("body").trigger("refresh");

});