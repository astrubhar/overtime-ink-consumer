$(document).ready(function () {


	/**
	 *
	 *  Interaction Events
	 *
	**/

	// Register Button
	$("[data-action=register]").on("click", function (event) {

		// Get Values
		values = $("#registerForm").triggerHandler("get");

		// Send Request
		$.ajax({
			type: "POST",
			url: "session_json/register/",
			data: values,
			dataType: "json",
			success: function(data) {

				// Errors?
				if (data.errors.length > 0) {

					// Clear Errors
					$("#registerForm").find(".form-group").removeClass("has-error");

					// Iterate Errors
					$.each(data.errors, function(index, value) {

						// Mark Error
						$("#registerForm").find("#form_" + value).parents(".form-group").addClass("has-error");

					});

				}

				// Success?
				else {

					// Show Success Modal
					$("#successModal").modal({
						"backdrop": "static",
						"keyboard": true
					});

				}

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	/**
	 *
	 *  View Controllers
	 *
	**/

	// Register Form -> Get Values
	$("#registerForm").on("get", function (event) {

		// Return Values
		return {
			"email"              : $(this).find("#form_email").val(),
			"password"           : $(this).find("#form_password").val(),
			"confirm_password"   : $(this).find("#form_confirm_password").val(),
			"firstname"          : $(this).find("#form_firstname").val(),
			"lastname"           : $(this).find("#form_lastname").val(),
			"company"            : $(this).find("#form_company").val(),
			"phone"              : $(this).find("#form_phone").val().replace(/[^\d]/g, ''),
			"phone_ext"          : $(this).find("#form_phone_ext").val(),
			"address"            : $(this).find("#form_address").val(),
			"city"               : $(this).find("#form_city").val(),
			"state"              : $(this).find("#form_state").val(),
			"postcode"           : $(this).find("#form_postcode").val(),
		};

	});

});