$(document).ready(function () {

	/**
	 *
	 *  Intervals
	 *
	**/

	// Every Minute
	setInterval(function () {

		// Refresh Page
		$("body").trigger("refresh");

	}, 60000);

	/**
	 *
	 *  Interaction Events
	 *
	**/

	// Create Button
	$(document).on("click", "[data-action=create]", function (event) {

		// Show Modal
		$("#modifyModal").trigger("show", [{}]);

		// Cancel Event
		event.preventDefault();

	});

	// View Button
	$(document).on("click", "[data-action=view]", function (event) {

		// Send Request
		$.ajax({
			url: "orders_json/edit/" + $(this).attr("data-id"),
			dataType: "json",
			success: function(response) {

				// Show Modal
				$("#viewModal").trigger("show", [response.order, response.attachments, response.items]);

			}
		});

		// Cancel Event
		event.preventDefault();

	});

	// Add Cart Item Button
	$(document).on("click", "[data-action=add_item]", function (event) {

		// Clear Errors
		$("#cart_list tfoot .form-group").removeClass("has-error");

		// Show Errors
		$("#cart_list tfoot .form-control").filter(function() { return this.value == ""; }).parents(".form-group").addClass("has-error");

		// Error free?
		if ($("#cart_list tfoot .form-group.has-error").length < 1) {

			// Add Row
			$("#form_attachment_input").trigger("add_row", [{
				"id"            : "",
				"quantity"      : $("#cart_list #form_quantity").val(),
				"decoration"    : $("#cart_list #form_decoration").val(),
				"color"         : $("#cart_list #form_color").val(),
				"garment"       : $("#cart_list #form_garment").val(),
				"artwork"       : $("#cart_list #form_artwork").val()
			}]);

			// Clear Form
			$("#cart_list tfoot .form-control").val("").css({'background-color': '#FFFFFF', 'color': '#000000'});

		}

		// Cancel Event
		event.preventDefault();

	});

	// Remove Cart Item Button
	$(document).on("click", "[data-action=remove_item]", function (event) {

		// Confirm
		if (confirm("Are you sure?")) {

			// Remove Row
			$(this).parents("tr").remove();

		}

		// Cancel Event
		event.preventDefault();

	});

	// Save Button
	$("[data-action=save]").on("click", function (event) {

		// Cancel Event
		event.preventDefault();

		// Get Values
		values = $("#modifyModal").triggerHandler("get");

		// Has cart items?
		if ($("#cart_list tbody tr").length < 1) {

			// Show Errors
			$("[data-action=add_item]").trigger("click");

			// Return
			return;

		}

		// Send Request
		$.ajax({
			type: "POST",
			url: "orders_json/save/" + values.id,
			data: values,
			dataType: "json",
			success: function(data) {

				// Errors?
				if (data.errors.length > 0) {

					// Clear Errors
					$("#modifyModal").find(".form-group").removeClass("has-error");

					// Iterate Errors
					$.each(data.errors, function(index, value) {

						// Mark Error
						$("#modifyModal").find("#form_" + value).parents(".form-group").addClass("has-error");

					});

				}

				// Success?
				else {

					// Get Items
					items = $("#form_attachment_input").triggerHandler("get");

					// Iterate Items
					$.each(items, function(index, value) {

						// Needs ID?
						if (value.id == "") {

							// Send Request
							$.ajax({
								type: "POST",
								url: "orders_json/save_cart/" + data.id,
								data: value,
								dataType: "json"
							});

						}

					});

					// Refresh Page
					$("body").trigger("refresh");

					// Dismiss Modal
					$("#modifyModal").modal("hide");

					// Set Order ID
					$("#successModal .order_id").html(data.id);

					// Show Success
					$("#successModal").modal("show");

				}

			}
		});

	});

	// Address is Changed
	$(document).on("keyup", "#form_address,#form_city,#form_state,#form_postcode", function () {

		// Clear Toggle
		set_toggle_state($("a[data-value=same_address]"), false);

	});

	// Same Address is Clicked
	$(document).on("click", "a[data-value=same_address]", function () {

		// On?
		if ($(this).hasClass("btn-success")) {

			// Fetch Address
			$.ajax({
				type: "GET",
				url: "orders_json/get_address",
				dataType: "json",
				success: function(response) {

					$("#form_address").val(response.address);
					$("#form_city").val(response.city);
					$("#form_state").val(response.state);
					$("#form_postcode").val(response.postcode);

				}
			});

		}

	});

	// Detect File Upload
	$(document).on("change", "#form_attachment_input", function () {

		// Copy
		that = this;

		// Has file?
		if ($(this).val().length > 0) {

			// Submit Form
			$("form.upload").ajaxForm({
				dataType: "json",
				success: function (data) {

					// Success?
					if (data.id > 0) {

						// Add File
						$(that).parents("li.input").before(
							'<li class="list-group-item" data-id="' + data.id + '"><a href="' + $("input[name=company_path]").val() + 'uploads/' + data.location + '" target="_blank">' + data.name + ' (' + data.extension + '; ' + data.size + ' MB)</a><a href="#" class="pull-right" data-action="remove_upload"><span class="glyphicon glyphicon-remove"></span></a></li>'
						);

						// Set Control
						control = $(that);

						// Reset Upload
						control.replaceWith(control = control.clone(true));

					}

				}
			}).submit();

		}

	});

	// Remove Upload Button
	$(document).on("click", "[data-action=remove_upload]", function (event) {

		// Copy
		that = this;

		// Confirm
		if (confirm("Are you sure?")) {

			// Set ID
			id = $(this).parents("li").attr("data-id");

			// Send Request
			$.ajax({
				url: "orders_json/delete_upload/" + id,
				dataType: "json",
				success: function(response) {

					// Remove Row
					$(that).parents("li").remove();

				}
			});

		}

		// Cancel Event
		event.preventDefault();

	});

	// Approve Proof of Concepts
	$(document).on("click", "#approve_concepts", function (event) {

		// Copy
		that = this;

		// Confirm
		if (confirm("We will begin production after these proof of concepts are approved. Are you sure you want to approve this?")) {

			// Set ID
			id = $("#viewModal input#view_id").val();

			// Send Request
			$.ajax({
				url: "orders_json/approve_concepts/" + id,
				dataType: "json",
				success: function(response) {

					// Success?
					if (response.errors.length < 1) {

						// Remove Row
						$(that).attr("disabled", "disabled");

					}

				}
			});

		}

		// Cancel Event
		event.preventDefault();

	});

	// Has show ID?
	if ($("input[name=show_id]").val().length > 0) {

		// Send Request
		$.ajax({
			url: "orders_json/edit/" + $("input[name=show_id]").val(),
			dataType: "json",
			success: function(response) {

				// Show Modal
				$("#viewModal").trigger("show", [response.order, response.attachments, response.items]);

			}
		});

	}

	/**
	 *
	 *  View Controllers
	 *
	**/

	// Refresh
	$("body").on("refresh", function (event) {

		$.ajax({
			type: "GET",
			url: $("input[name=refresh_url]").val(),
			data: {},
			dataType: "text",
			success: function(html) {

				// Replace Title
				$("title").text(html.match("<title>(.*?)</title>")[1].replace("&#187;", "»"));

				// Parse HTML DOM (Strips <head>)
				html = $.parseHTML(html);

				// Replace Pill Navigation
				$("ul.nav.nav-tabs").replaceWith($(html).find("ul.nav.nav-tabs"));

				// Replace Table
				$("table#list").replaceWith($(html).find("table#list"));

				// Replace Pagination
				$("ul.pagination").replaceWith($(html).find("ul.pagination"));

				// Count Rows in Table
				if ($("table.table tr[data-id]").length > 0) {

					// Hide Not Found
					$("#not_found").addClass("hidden");

				} else {

					// Show Not Found
					$("#not_found").removeClass("hidden");

				}

				// Highlight Search Terms
				highlightSearchWords();

			}
		});

	});

	// Modify Modal -> Show
	$("#modifyModal").on("show", function (event) {

		// Remove Errors
		$(this).find(".form-group").removeClass("has-error");

		// Clear Cart
		$("#cart_list tbody tr").remove();

		// Clear Form Controls
		$("#cart_list tfoot .form-control").val("");

		// Remove Attachments
		$("#attachments li:not(.input),#concepts li:not(.input)").remove();

		// Reset Form
		$(this).find("input,select,textarea").val("");
		$(this).find("#form_status [data-value=ordered] button").trigger("click");

		// Set Verified Toggle
		set_toggle_state($(this).find("a[data-value=same_address]"), false);

		// Set Empty Dates
		$(this).find("#form_created,#form_updated").html("No Date");

		// Clear Shipping
		$(this).find("#form_shipped").data("DateTimePicker").setDate("");

		// Show
		$(this).modal();

	});

	// Modify Modal -> Get Values
	$("#modifyModal").on("get", function (event) {

		// Return Values
		return {
			"id"             : "",
			"address"        : $(this).find("#form_address").val(),
			"city"           : $(this).find("#form_city").val(),
			"state"          : $(this).find("#form_state").val(),
			"postcode"       : $(this).find("#form_postcode").val(),
			"customer_notes" : $(this).find("#form_customer_notes").val(),
			"attachments"    : $("#attachments").triggerHandler("get"),
			"shipped"        : ($(this).find("#form_shipped input").val().length < 1 ? null : $(this).find("#form_shipped").data("DateTimePicker").getDate().format("YYYY-MM-DD HH:mm:ss"))
		};

	});

	// View Modal -> Show
	$("#viewModal").on("show", function (event, values, attachments, items) {

		// Clear Cart
		$("#view_cart_list tbody tr").remove();

		// Clear Form Controls
		$("#view_cart_list tfoot .form-control").val("");

		// Remove Attachments
		$("#view_attachments li:not(.input),#view_concepts li:not(.input)").remove();

		// Set Values
		$(this).find("span#view_id").html(values.id);
		$(this).find("input#view_id").val(values.id);
		$(this).find("#view_address").html(values.address);
		$(this).find("#view_city").html(values.city);
		$(this).find("#view_state").html(values.state);
		$(this).find("#view_postcode").html(values.postcode);
		$(this).find("#view_status").html(values.status);
		$(this).find("#view_customer_notes").html($("<div />").html(values.customer_notes == "" ? "&mdash;" : values.customer_notes).text());
		$(this).find("#view_created").html(values.created);
		$(this).find("#view_shipped").html(values.shipped);
		$(this).find("#view_shipped_label").html(values.status == "shipped" ? "Shipped" : "Shipping");
		$(this).find("#view_scheduled").html((values.start != null && values.end != null ? values.start + " - <br>" + values.end : "Not Yet Scheduled"));

		// Iterate Attachments
		$.each(attachments, function(index, data) {

			// Add File
			$((data.type == "attachment" ? "#view_attachments" : "#view_concepts")).append(
				'<li class="list-group-item" data-id="' + data.id + '"><a href="' + $("input[name=company_path]").val() + 'uploads/' + data.location + '" target="_blank">' + data.name + " (" + data.extension + '; ' + data.size + ' MB)</a></li>'
			);

		});

		// Show Review
		$("#approve_concepts").parents(".form-group").show();

		// Has nothing to review?
		if ($("#view_concepts li").length < 1) {

			// Hide
			$("#approve_concepts").parents(".form-group").hide();

		}

		// Already reviewed?
		else if ($.inArray("approved", values.options) > -1) {

			// Disable
			$("#approve_concepts").attr("disabled", "disabled");

		} else {

			// Enable
			$("#approve_concepts").removeAttr("disabled");

		}

		// No attachments?
		if ($("#view_attachments li").length < 1) {

			// Add File
			$("#view_attachments").append(
				'<li class="list-group-item" style="font-style: italic;">&mdash; None &mdash;</li>'
			);

		}

		// No concepts?
		if ($("#view_concepts li").length < 1) {

			// Add File
			$("#view_concepts").append(
				'<li class="list-group-item" style="font-style: italic;">&mdash; None &mdash;</li>'
			);

		}

		// Iterate Cart Items
		$.each(items, function(index, value) {

			// Add Row
			$("#form_attachment_input").trigger("add_row", [value, false, "#view_cart_list"]);

		});

		// Show
		$(this).modal();

	});

	// Cart Modal -> Add Row
	$("#form_attachment_input").on("add_row", function (event, values, with_remove, using) {

		with_remove = (typeof with_remove == "undefined" ? true : false);
		using       = (typeof using == "undefined" ? "#cart_list" : using);

		// Set Decoration Text
		switch (values.decoration) {
			case "screen-printing":
				decoration = "Screen Printing";
				break;
			case "embroidery":
				decoration = "Embroidery";
				break;
			case "vinyl":
				decoration = "Vinyl";
				break;
		}

		// Set Artwork Text
		switch (values.artwork) {
			case "provided":
				artwork = "I'll Attach Artwork";
				break;
			case "in-house":
				artwork = "Create In-House";
				break;
		}

		// Create Row
		row  = '<tr data-id="' + values.id + '">';
		row += '    <td>' + (with_remove ? '<a href="#" data-action="remove_item"><i class="glyphicon glyphicon-remove btn-sm"></i></a>' : '') + '</td>';
		row += '    <td>' + values.quantity + '</td>';
		row += '    <td>' + values.garment + '</td>';
		row += '    <td><span style="display: inline-block; margin-bottom: -1px; background-color: ' + values.color + '; width: 12px; height: 12px; border-radius: 6px;"></span> ' + values.color + '</td>';
		row += '    <td>' + decoration + '</td>';
		row += '    <td>' + artwork + '</td>';
		row += '    <td></td>';
		row += '</tr>';

		// Attach
		$(using + " tbody").append(row);

		// Set Values
		$(using + " tbody tr:not(.input)").last().data("values", values);

	});

	// Cart Modal -> Get Values
	$("#form_attachment_input").on("get", function (event) {

		// Define
		items = [];

		// Iterate Cart
		$("#cart_list tbody tr").each(function () {

			// Get Values
			var values = $(this).data("values");

			// Add Array Item
			items.push({
				"id"            : values.id,
				"quantity"      : values.quantity,
				"decoration"    : values.decoration,
				"color"         : values.color,
				"garment"       : values.garment,
				"artwork"       : values.artwork
			});

		});

		// Return Values
		return items;

	});

	// Attachments -> Get Values
	$(".interactive-list").on("get", function (event) {

		// Define
		items = [];

		// Iterate Cart
		$("#attachments li:not(.input)").each(function () {

			// Add Array Item
			items.push($(this).attr("data-id"));

		});

		// Return Values
		return items;

	});

});