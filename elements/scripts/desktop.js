$(function () {

	// Highlight Search Terms
	highlightSearchWords();

	// Toggle Buttons with Checkmarks
	$("[data-action=toggle]").on("click", function (event) {

		// Off?
		if ($(this).hasClass("btn-default") || $(this).hasClass("btn-danger")) {

			// Set Enabled
			set_toggle_state($(this), true);

		} else {

			// Set Disabled
			set_toggle_state($(this), false);

		}

		// Stop Click Event
		event.preventDefault();

	});

	// Detect Filter Redirects
	$(document).on("change", "[data-action=filter]", function () {

		// Redirect
		window.location.href = $(this).attr("data-url").replace("%?", $(this).val());

	});

	// Has refresh URL and search?
	if ($("input[name=refresh_url]").length > 0 && $("input[name=find]").length > 0) {

		// Set URL with Filters
		$("input[name=find]").parents("form[role=search]").attr("action", $("input[name=refresh_url]").val());

	}

	// Toggle Switch -> Click
	$("div.toggle-switch button,div.toggle-switch a").on("click", function (event) {

		// Unselect
		$(this).parents(".toggle-switch").find("button,a").removeClass("btn-primary active").addClass("btn-default");

		// Reselect
		$(this).removeClass("btn-default").addClass("btn-primary active");

		// Trigger Change
		$(this).parents(".toggle-switch").trigger("change", [$(this).attr("data-value")]);

		// Prevent Default
		event.preventDefault();

	});

	// Toggle Switch -> Get Value
	$("div.toggle-switch").on("get", function (event) {

		// Return Value
		return $(this).find(".active").attr("data-value");

	});

	// Step Wizard -> Click
	$("div.stepwizard button").on("click", function (event) {

		// Deselect
		$(this).parents(".stepwizard-row").find("button").removeClass("btn-success btn-warning active").addClass("btn-default");

		// Hidden?
		if ($(this).parents(".stepwizard-step").hasClass("disabled")) {

			// Use Warning
			$(this).removeClass("btn-default").addClass("btn-warning active");

		} else {

			// Use Success
			$(this).removeClass("btn-default").addClass("btn-success active");

		}

		// Trigger Change
		$(this).parents("div.stepwizard").trigger("change", [$(this).parents(".stepwizard-step").attr("data-value"), $(this).parents(".stepwizard-step").hasClass("disabled")]);

		// Prevent Default
		event.preventDefault();

	});

	// Step Wizard -> Get Value
	$("div.stepwizard").on("get", function (event) {

		// Return Value
		return $(this).find(".active").parents(".stepwizard-step").attr("data-value");

	});

	// Find AutoComplete -> Clear Value
	$(".find button[data-action=clear]").on("click", function (event, focus) {

		// Set Default
		focus = (typeof focus === "undefined" ? true : focus);

		// Clear ID
		$(this).parents(".find").attr("data-id", "");

		// Disable Remove
		$(this).removeClass("btn-primary").addClass("btn-default").attr("disabled", "disabled");

		// Clear Label and Enable Input
		if (focus) $(this).parents(".find").find("input[type=text]").val("").removeAttr("disabled").focus();

		// Clear Description
		$(this).parents(".find").next().find(".col-sm-9").html("");

		// Prevent Default
		event.preventDefault();

	});

	// Find AutoComplete -> Set Value
	$(".find").on("set", function (event, id, label) {

		// Set ID
		$(this).attr("data-id", id);

		// Set Label and Disable Input
		$(this).find("input[type=text]").val(label).attr("disabled", "disabled");

		// Enable Remove
		$(this).find("button[data-action=clear]").removeClass("btn-default").addClass("btn-primary").removeAttr("disabled");

	});

	// Find AutoComplete -> Get Value
	$(".find").on("get", function (event) {

		// Return Value
		return $(this).attr("data-id");

	});

	// Create Date/Time Pickers
	$(".input-group.date").datetimepicker({
		pickDate           : true,
		pickTime           : true,
		useMinutes         : true,
		useSeconds         : false,
		useCurrent         : false,
		minuteStepping     : 1,
		minDate            : moment(),
		maxDate            : "1/1/2099",
		showToday          : true,
		language           : "en",
		defaultDate        : "",
		useStrict          : false,
		sideBySide         : false,
		daysOfWeekDisabled : [],
	});

	// Create Color Picker
	$(".color-picker").ColorPickerSliders({
		placement: 'right',
		color: '#FFFFFF',
		size: 'large',
		swatches: [
			'#383435','#65696C','#E3E7E6','#FFFFFF','#9D3141',
			'#EE4B44','#8F3431','#FEF8DE','#E4C3A0','#CBB87D',
			'#F6B1C4','#F494B0','#EE367C','#E47736','#F37A45',
			'#F89C39','#FCB72E','#A06D38','#625231','#D3A239',
			'#FCD226','#FBE82A','#25894B','#305536','#61733F',
			'#A0CF6F','#B4DFD4','#C3E4DD','#303D5F','#285EA8',
			'#24BDE6','#80BFEA','#2B7EC0','#623B50','#583E87',
			'#827CBA','#A1378F'
		],
		customswatches: false,
		sliders: false,
		order: {
			rgb: 1
		},
		hsvpanel: true
	});

});

/**
 *
 *  Search Box Highlighter
 *
**/

function highlightSearchWords() {

	if ($("input[name=find]").length != 0 && $("input[name=find]").val() != "") {
		var terms = $("input[name=find]").val().split(" ");
		for (var i = 0; i < terms.length; i++) {
			$("table tr td,div.list-group").highlight(terms[i]);
		}
	}
	if ($("input[name=highlight_terms]").length != 0 && $("input[name=highlight_terms]").val() != "") {
		var terms = $("input[name=highlight_terms]").val().split(",");
		for (var i = 0; i < terms.length; i++) {
			$("table tr td,div.list-group").highlight(terms[i]);
		}
	}

}

/**
 *
 *  Toggle Button Checked?
 *
**/

function set_toggle_state(element, enabled) {

	// Enabled?
	if (enabled) {

		// Enable
		$(element).removeClass("btn-default btn-danger").addClass("btn-success").find("i").removeClass("glyphicon-unchecked").addClass("glyphicon-check");

	} else {

		// Disable
		$(element).removeClass("btn-success").addClass("btn-default").find("i").removeClass("glyphicon-check").addClass("glyphicon-unchecked");

	}

}

function is_toggle_checked(element) {

	// On?
	if ($(element).hasClass("btn-success")) {

		return true;

	}

	// Off?
	else {

		return false;

	}

}