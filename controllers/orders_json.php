<?php

class Orders_JSON extends Foundation {

	function edit($id = null) {

		// Instantiate
		$order        = new Orders_model($id);
		$attachments  = new Attachments_model();
		$frequency    = new Frequency();
		$timeago      = new TimeAgo($frequency->application->timezone);

		// Format Phone Number
		$order->user_id->phone = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $order->user_id->phone);

		// Expand Country and State
		list($country, $state) = explode("-", $order->user_id->state);

		// Set Customer Input Label
		$order->user_id->label = "{$order->user_id->firstname} {$order->user_id->lastname}, {$order->user_id->address}, {$order->user_id->city}, {$state}, {$country}";
		$order->user_id->desc  = "<strong>{$order->user_id->firstname} {$order->user_id->lastname}</strong><br>{$order->user_id->address}<br>{$order->user_id->city}, {$state} {$order->user_id->postcode}, {$country}<br>" . preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $order->user_id->phone);

		// Remove Password
		unset($order->user_id->password);

		// Define
		$files = array();

		// Find Matches
		$matches = $attachments->find(1, "", 9999, array(
			"`attachments`.`order_id` = '" . mysql_real_escape_string($id) . "'",
		));

		// Iterate
		foreach ($matches as $match) {

			// Add
			$files[] = $match->toArray();

		}

		// Uppercase Status
		$order->status = ucwords($order->status);

		// Convert Dates
		$order->start                 = ($order->start == "0000-00-00 00:00:00" ? null : date("m/d/Y g:i A", strtotime($order->start)));
		$order->end                   = ($order->end == "0000-00-00 00:00:00" ? null : date("m/d/Y g:i A", strtotime($order->end)));
		$order->shipped               = ($order->shipped == "0000-00-00 00:00:00" ? null : date("m/d/Y g:i A", strtotime($order->shipped)));
		$order->created               = $timeago->inWords($order->created, "now");
		$order->updated               = $timeago->inWords($order->updated, "now");
		$order->user_id->last_login   = $timeago->inWords($order->user_id->last_login, "now");

		// Return
		echo json_encode(array(
			"order"          => $order->toArray(),
			"attachments"    => $files,
			"items"          => $this->edit_cart($id),
		));

	}

	private function edit_cart($id = null) {

		// Instantiate
		$items = new Items_model();

		// Define
		$response = array();

		// Find Matches
		$matches = $items->find(1, "", 9999, array(
			"`items`.`order_id` = '" . mysql_real_escape_string($id) . "'",
		));

		// Iterate Responses
		foreach ($matches as $match) {

			// Remove Order
			unset($match->order_id);

			// Set Response
			$response[] = $match->toArray();

		}

		// Return
		return $response;

	}

	function save() {

		// Instantiate
		$order = new Orders_model();

		// Read User
		$order->user_id->read($_SESSION["id"]);

		// Set Values
		$order->address           = $_POST["address"];
		$order->city              = $_POST["city"];
		$order->state             = $_POST["state"];
		$order->postcode          = $_POST["postcode"];
		$order->customer_notes    = $_POST["customer_notes"];
		$order->shipped           = (empty($_POST["shipped"]) ? null : $_POST["shipped"]);
		$order->status            = "ordered";

		// Set Created Date
		$order->created = date("Y-m-d H:i:s");

		// Save
		$errors = $order->save();

		// No errors?
		if (empty($errors)) {

			// Iterate Attachments
			foreach ($_POST["attachments"] as $attachment_id) {

				// Instantiate
				$attachment = new Attachments_model($attachment_id);

				// Read Order ID
				$attachment->order_id = $order->id;

				// Save
				$attachment->save();

			}

		}

		// Save and Return
		echo json_encode(array(
			"id"     => $order->id,
			"errors" => $errors,
		));

	}

	function save_cart($order_id) {

		// Instantiate
		$item = new Items_model();

		// Set
		$item->quantity   = $_POST["quantity"];
		$item->decoration = $_POST["decoration"];
		$item->color      = $_POST["color"];
		$item->garment    = $_POST["garment"];
		$item->artwork    = $_POST["artwork"];

		// Read Order
		$item->order_id->read($order_id);

		// Save and Return
		echo json_encode(array(
			"errors" => $item->save(),
		));

	}

	function delete_upload($id) {

		// Instantiate
		$attachment = new Attachments_model($id);

		// Delete and Return
		echo json_encode(array(
			"status" => $attachment->delete(),
		));

	}

	function get_address() {

		// Instantiate
		$user = new Users_model($_SESSION["id"]);

		// Return
		echo json_encode(array(
			"address"    => $user->address,
			"city"       => $user->city,
			"state"      => $user->state,
			"postcode"   => $user->postcode,
		));

	}

	function upload_file() {

		// Instantiate
		$attachments = new Attachments_model();

		// Attachment?
		if (isset($_FILES["file_attachment"])) {

			// Upload File
			$attachments->upload($_FILES["file_attachment"], "attachment");

		}

		// Return Result
		echo json_encode($attachments->toArray());

	}

	function approve_concepts($id) {

		// Instantiate
		$order = new Orders_model($id);

		// Needs flag?
		if (!in_array("approved", $order->options)) {

			// Set Option
			$order->options[] = "approved";

		}

		// Save and Return
		echo json_encode(array(
			"errors" => $order->save(),
		));

	}

}