<?php

class Orders extends Desktop {

	static $selected_navigation = "orders";

	public function index($status = "all", $page = 1, $find = "", $show_id = "") {

		// Search?
		if (isset($_POST["find"])) {

			// Redirect
			$this->redirect("orders/index/{$status}/1/" . urlencode($_POST["find"]));

		}

		// Instantiate
		$frequency    = new Frequency();
		$timeago      = new TimeAgo($frequency->application->timezone);
		$orders       = new Orders_model();
		$view         = new View("orders/index");

		// URL Decode Find
		$find = trim(str_replace("+", " ", urldecode($find)));

		// Sort
		$orders->order("orders", "created", "desc");

		// Define
		$conditions = array();

		// Only View Own Jobs
		$conditions[] = "`orders`.`user_id` = '" . mysql_real_escape_string($_SESSION["id"]) . "'";

		// Has status?
		if ($status != "all") {

			// Filter
			$conditions[] = "`orders`.`status` = '" . mysql_real_escape_string($status) . "'";

		}

		// Find Matches
		$view->orders = $orders->find($page, $find, $frequency->application->results_per_page, $conditions);

		// Calculate Pages
		$orders->pagination($page);

		// Set Pagination
		$view->status         = $status;
		$view->show_id        = $show_id;
		$view->pages          = $orders->abstraction_pages;
		$view->total          = $orders->abstraction_count;
		$view->page           = $page;
		$view->start          = $orders->pagination_start;
		$view->end            = $orders->pagination_end;
		$view->counts         = $orders->counts();
		$view->find           = $find;
		$view->timeago        = $timeago;
		$view->company_path   = $frequency->application->path->company_desktop;
		$view->highlight      = (!empty($find) ? str_replace(" ", ",", $find) : "");

		// Render
		$view->render();

		// Set Title and Search Term
		$this->title = "Orders (" . count($view->orders) . " of {$orders->abstraction_count})";
		$this->find  = $find;

	}

	public function review($id, $checksum) {

		// Instantiate
		$order = new Orders_model($id);

		// Confirm
		$status = $order->confirm_review($checksum);

		// Valid?
		if ($status) {

			// Redirect
			$this->redirect("orders/index/all/1/+/{$id}");

		} else {

			// Redirect
			$this->redirect("orders/index");

		}


	}

}