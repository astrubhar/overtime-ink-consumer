<?php

class Calendar extends Desktop {

	static $selected_navigation = "calendar";

	public function index($status = "all", $page = 1, $find = "") {

		// Search?
		if (isset($_POST["find"])) {

			// Redirect
			$this->redirect("calendar/index/{$status}/1/" . urlencode($_POST["find"]));

		}

		// Instantiate
		$frequency    = new Frequency();
		$timeago      = new TimeAgo($frequency->application->timezone);
		$orders       = new Orders_model();
		$items        = new Items_model();
		$view         = new View("calendar/index");

		// URL Decode Find
		$find = str_replace("+", " ", urldecode($find));

		// Sort
		$orders->order("orders", "created", "desc");

		// Define
		$conditions = array();

		// Set Pagination
		$view->find      = $find;
		$view->timeago   = $timeago;
		$view->highlight = (!empty($find) ? str_replace(" ", ",", $find) : "");

		// Render
		$view->render();

		// Set Title and Search Term
		$this->title = "Calendar";

	}

}