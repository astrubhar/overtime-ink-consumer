<?php

class Foundation {

	function __construct() {

		// Load Configuration
		$frequency = new Frequency();

		// Set Session Timeout to One Day
		session_set_cookie_params($frequency->application->session_length);

		// Set PHP Configuration
		ini_set("session.gc_maxlifetime", $frequency->application->session_length);

		//ini_set("session.gc_maxlifetime", $Lifetime);
		ini_set("session.gc_divisor", "1");
		ini_set("session.gc_probability", "1");
		ini_set("session.cookie_lifetime", "0");
		ini_set("session.save_path", "{$frequency->frequency->path}/sessions");

		// Start Session
		session_start();

		// Set Path
		$this->path = $frequency->application->path->desktop;

		// Set Request
		$request = $frequency->parse($_SERVER["QUERY_STRING"]);

		// Authenticate
		$this->authenticate($request);

	}

	private function authenticate($request) {

		// Do they need to authenticate?
		if ((!isset($_SESSION["id"]) or $_SESSION["id"] < 1) and $request[0] != "session" and $request[0] != "session_json") {

			// Redirect
			$this->redirect("session/authorize/" . base64_encode(implode("/", $request)));

		}

	}

	function redirect($location, $with_path = true) {

		// Erase Output
		ob_end_clean();
		ob_start();

		// Redirect
		header("Location: " . ($with_path ? $this->path : "") . "{$location}");
		header("Connection: close");
		die();

	}

}