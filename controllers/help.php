<?php

class Help extends Desktop {

	static $selected_navigation = "help";

	function index($flag = "", $errors = array()) {

		// Instantiate
		$frequency    = new Frequency();
		$users        = new Users_model();
		$view         = new View("help/index");

		// Render
		$view->render();

		// Set Title
		$this->title = "Help";

	}

}