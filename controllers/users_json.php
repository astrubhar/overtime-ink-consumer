<?php

class Users_JSON extends Foundation {

	function edit($id = null) {

		// Instantiate
		$user         = new Users_model($id);
		$frequency    = new Frequency();
		$timeago      = new TimeAgo($frequency->application->timezone);

		// Format Phone Number
		$user->phone = preg_replace('/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $user->phone);

		// Replace Password
		$user->password = (empty($user->password) ? false : true);

		// Set Role
		$user->role = ($_SESSION["role"] == "administrator" ? $user->role : "customer");

		// Convert Dates
		$user->created    = $timeago->inWords($user->created, "now");
		$user->updated    = $timeago->inWords($user->updated, "now");
		$user->last_login = $timeago->inWords($user->last_login, "now");

		// Return
		echo json_encode($user->toArray());

	}

	function save($id = null) {

		// Instantiate
		$user = new Users_model($id);

		// Set Values
		$user->email      = $_POST["email"];
		$user->firstname  = $_POST["firstname"];
		$user->lastname   = $_POST["lastname"];
		$user->company    = $_POST["company"];
		$user->phone      = $_POST["phone"];
		$user->phone_ext  = $_POST["phone_ext"];
		$user->address    = $_POST["address"];
		$user->city       = $_POST["city"];
		$user->state      = $_POST["state"];
		$user->postcode   = $_POST["postcode"];
		$user->notes      = $_POST["notes"];
		$user->role       = ($_SESSION["role"] == "administrator" ? $_POST["role"] : "customer");
		$user->options    = $_POST["options"];

		// New?
		if (is_null($user->id)) {

			// Set Created Date
			$user->created = date("Y-m-d H:i:s");

		} else {

			// Set Updated Date
			$user->updated = date("Y-m-d H:i:s");

		}

		// Set Password?
		if (!empty($_POST["password"])) {

			// Erase?
			if ($_POST["password"] == "NULL") {

				$user->password = null;

			} else {

				// Generate Hash
				$user->password = $user->generate_hash($_POST["email"], $_POST["password"]);

			}

		}

		// Save and Return
		echo json_encode(array(
			"errors" => $user->save(),
		));

	}

	function delete($id) {

		// Instantiate
		$user = new Users_model($id);

		// Delete and Return
		echo json_encode(array(
			"status" => $user->delete(),
		));

	}

}