<?php

class Session extends Desktop {

	static $selected_navigation = "";

	function authorize($redirect = "none", $flag = "", $errors = array()) {

		// Instantiate
		$frequency    = new Frequency();
		$users        = new Users_model();
		$view         = new View("session/authorize");

		// Submission?
		if (isset($_POST["email"])) {

			// Attempt to Authenticate
			$id = $users->id_by_authentication($_POST["email"], $_POST["password"]);

			// Valid credentials?
			if (is_numeric($id)) {

				// Read
				$users->read($id);

				// Needs to verify email?
				if (!in_array("verified", $users->options)) {

					// Resend Verification Email
					$users->verification_email();

					// Redirect with Tag
					$this->redirect("session/authorize/{$redirect}/verify");

				} else {

					// Update Last Login
					$users->last_login = date("Y-m-d H:i:s");

					// Save
					$users->save();

					// Set ID
					$_SESSION["id"]     = $users->id;
					$_SESSION["role"]   = $users->role;

					// Has redirect URL?
					if ($redirect != "none") {

						// Redirect
						$this->redirect(base64_decode($redirect));

					} else {

						// Redirect to Default Page
						$this->redirect($frequency->application->default_page);

					}

				}

			}

			// Failed -- Add Errors
			$errors[]   = "email";
			$errors[]   = "password";

		}

		// Set Variables
		$view->redirect = $redirect;
		$view->flag     = $flag;
		$view->errors   = $errors;

		// Render
		$view->render();

		// Set Title
		$this->title = "Please Sign In";

	}

	function register($errors = array()) {

		// Instantiate
		$frequency    = new Frequency();
		$users        = new Users_model();
		$view         = new View("session/register");

		// Render
		$view->render();

		// Set Title
		$this->title = "Register";

	}

	function confirm($id, $checksum) {

		// Instantiate
		$user = new Users_model($id);

		// Confirm
		$status = $user->confirm_email($checksum);

		// Redirect
		$this->redirect("session/authorize/none/" . ($status ? "verified" : "invalid"));

	}

	function destroy() {

		// Destory
		session_destroy();

		// Redirect
		$this->redirect("session/authorize");

	}

}