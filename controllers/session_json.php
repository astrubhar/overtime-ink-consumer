<?php

class Session_JSON extends Foundation {

	function register() {

		// Instantiate
		$user = new Users_model();

		// Set Values
		$user->email      = ($user->email_exists($_POST["email"]) ? "" : $_POST["email"]);
		$user->firstname  = $_POST["firstname"];
		$user->lastname   = $_POST["lastname"];
		$user->company    = $_POST["company"];
		$user->phone      = (strlen($_POST["phone"]) == 10 ? $_POST["phone"] : "");
		$user->phone_ext  = $_POST["phone_ext"];
		$user->address    = $_POST["address"];
		$user->city       = $_POST["city"];
		$user->state      = $_POST["state"];
		$user->postcode   = (strlen($_POST["postcode"]) >= 5 ? $_POST["postcode"] : "");

		// Set Role to Customer
		$user->role = "customer";

		// Set Created Date
		$user->created = date("Y-m-d H:i:s");

		// Define
		$errors = array();

		// No password? Too short (< 6 characters)?
		if (empty($_POST["password"]) or strlen($_POST["password"]) < 6) {

			// Trigger Error
			$errors[] = "password";

		}

		// Password confirm not match?
		elseif ($_POST["password"] != $_POST["confirm_password"]) {

			// Trigger Error
			$errors[] = "confirm_password";

		}

		// Password correct?
		else {

			// Generate Hash
			$user->password = $user->generate_hash($_POST["email"], $_POST["password"]);

		}

		// Save and Store Errors
		$errors = array_merge($errors, $user->save());

		// Success?
		if (empty($errors)) {

			// Send Verification Email
			$user->verification_email();

		}

		// Save and Return
		echo json_encode(array(
			"errors" => $errors,
		));

	}

}