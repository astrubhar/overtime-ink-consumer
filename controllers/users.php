<?php

class Users extends Desktop {

	static $selected_navigation = "users";

	public function index() {

		// Instantiate
		$frequency    = new Frequency();
		$user         = new Users_model();
		$view         = new View("users/index");

		// Read User
		$user->read($_SESSION["id"]);

		// Define
		$errors = array();

		// Post?
		if (isset($_POST["firstname"])) {

			// Make Phone Number Numeric
			$_POST["phone"] = preg_replace("/[^0-9]/", "", $_POST["phone"]);

			// Set Old Email
			$old_email = $user->email;

			// Set Values
			$user->email      = (($user->email_exists($_POST["email"]) and $user->email != $_POST["email"]) ? "" : $_POST["email"]);
			$user->firstname  = $_POST["firstname"];
			$user->lastname   = $_POST["lastname"];
			$user->company    = $_POST["company"];
			$user->phone      = (strlen($_POST["phone"]) == 10 ? $_POST["phone"] : "");
			$user->phone_ext  = $_POST["phone_ext"];
			$user->address    = $_POST["address"];
			$user->city       = $_POST["city"];
			$user->state      = $_POST["state"];
			$user->postcode   = (strlen($_POST["postcode"]) >= 5 ? $_POST["postcode"] : "");

			// Does old password match?
			if ($user->password == $user->generate_hash($old_email, $_POST["old_password"])) {

				// Set Updated Date
				$user->updated = date("Y-m-d H:i:s");

				// New password?
				if (!empty($_POST["password"])) {

					// Password too short (< 6 characters)?
					if (strlen($_POST["password"]) < 6) {

						// Trigger Error
						$errors[] = "password";

					}

					// Password confirm not match?
					elseif ($_POST["password"] != $_POST["confirm_password"]) {

						// Trigger Error
						$errors[] = "confirm_password";

					}

					// Password correct?
					else {

						// Generate Hash
						$user->password = $user->generate_hash($_POST["email"], $_POST["password"]);

					}

				} else {

					// Regenerate Old Hash
					$user->password = $user->generate_hash($_POST["email"], $_POST["old_password"]);

				}

				// Save and Store Errors
				$errors = array_merge($errors, $user->save());

			} else {

				// Trigger Error
				$errors[] = "old_password";

			}

		}

		// Set User and Errors
		$view->user   = $user;
		$view->errors = $errors;

		// Render
		$view->render();

		// Set Title and Search Term
		$this->title = "My Account";

	}

}